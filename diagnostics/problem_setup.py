#
# Import NumPy for array handling
#
import numpy as np
import math
#
# Import plotting libraries (start Python with ipython --matplotlib)
#
#from mpl_toolkits.mplot3d import axes3d
#from matplotlib import pyplot as plt
#
# Inputs:
# 80dens.dat
# 80f_d2g.dat
# 80temp.dat
# dustkappa_silicate.inp
#
# Some natural constants
#
au  = 1.49598e13     # Astronomical Unit       [cm]
pc  = 3.08572e18     # Parsec                  [cm]
ms  = 1.98892e33     # Solar mass              [g]
ts  = 5.78e3         # Solar temperature       [K]
ls  = 3.8525e33      # Solar luminosity        [erg/s]
rs  = 6.96e10        # Solar radius            [cm]
#
# Monte Carlo parameters
#
nphot    = 100000
#
# Grid parameters
#
nx       = 2560
ny       = 258       # not one sided
nz       = 1
#
# Model parameters
#
rin      = 0.052*au
rout     = 4*au
#
# Star parameters
#
mstar    = 1.82*ms
rstar    = 2.23*rs
tstar    = 7750
pstar    = [0.,0.,0.]
#
# Make the coordinates
#
# Note: The way the xi grid is made is slightly non-standard, but is
#       done this way to be consistent with problem_setup.pro (the IDL version)
#
xi       = np.linspace(rin,rout,nx+1)
y1       = np.linspace(math.pi/2.0-0.36,math.pi/2.0+0.36,ny)
y2       = np.insert(y1, ny//2, math.pi/2)   #insert the pi/2 surface
yi       = y2
zi       = np.array([0.,math.pi*2])
#xc       = 0.5e0 * ( xi[0:nx] + xi[1:nx+1] )
#yc       = 0.5e0 * ( yi[0:ny] + yi[1:ny+1] )
#
# Make the dust density model
#
#rr,tt    = np.meshgrid(xc,yc,indexing='ij')
#zzr      = math.pi/2.0 - tt
#rhod     = rho0 * (rr/au)**prho
#rhod     = rhod * np.exp(-0.50*(zzr/hpr)**2)
#
# Write the wavelength_micron.inp file
#
lam1     = 1.0e-1
lam2     = 100.0e0
#lam3     = 25.e0
#lam4     = 1.0e4
n12      = 50
#n23      = 100
#n34      = 30
lam      = np.logspace(np.log10(lam1),np.log10(lam2),n12,endpoint=True)
#lam23    = np.logspace(np.log10(lam2),np.log10(lam3),n23,endpoint=False)
#lam34    = np.logspace(np.log10(lam3),np.log10(lam4),n34,endpoint=True)
#lam      = np.concatenate([lam12,lam23,lam34])
nlam     = lam.size
#
# Write the wavelength file
#
with open('wavelength_micron.inp','w+') as f:
    f.write('%d\n'%(nlam))
    for value in lam:
        f.write('%13.6e\n'%(value))
#
#
# Write the stars.inp file
#
with open('stars.inp','w+') as f:
    f.write('2\n')
    f.write('1 %d\n\n'%(nlam))
    f.write('%13.6e %13.6e %13.6e %13.6e %13.6e\n\n'%(rstar,mstar,pstar[0],pstar[1],pstar[2]))
    for value in lam:
        f.write('%13.6e\n'%(value))
    f.write('\n%13.6e\n'%(-tstar))
#
# Write the grid file
#
with open('amr_grid.inp','w+') as f:
    f.write('1\n')                       # iformat
    f.write('0\n')                       # AMR grid style  (0=regular grid, no AMR)
    f.write('100\n')                     # Coordinate system
    f.write('0\n')                       # gridinfo
    f.write('1 1 0\n')                   # Include x,y,z coordinate
    f.write('%d %d %d\n'%(nx,ny,nz))     # Size of grid
    for value in xi:
        f.write('%15.8e\n'%(value))      # X coordinates (cell walls)
    for value in yi:
        f.write('%15.8e\n'%(value))      # Y coordinates (cell walls)
    for value in zi:
        f.write('%15.8e\n'%(value))      # Z coordinates (cell walls)
#
# Write the density file
#
with open('3800dens.dat') as f:  
    dens = np.loadtxt(f)
with open('3800f_d2g.dat') as f:  
    fd2g = np.loadtxt(f)
dustdens_SI = np.multiply(dens, fd2g)
dustdens = np.multiply(dustdens_SI, 1e-3)   #conversion from kg/m^3 to g/cm^3
dustdens2 = np.transpose(dustdens)
dustdens3 = np.concatenate((dustdens2[:ny//2][:], dustdens2[(ny//2)-1:][:]))
with open('dust_density.inp','w+') as f:
    f.write('1\n')                       # Format number
    f.write('%d\n'%(nx*ny*nz))           # Nr of cells
    f.write('1\n')                       # Nr of dust species
    data = dustdens3
    data.tofile(f, sep='\n', format="%13.6e")
    f.write('\n')
#
# Write the temperature file
#
with open('3800temp.dat') as f:  
    temp = np.loadtxt(f)
temp2 = np.transpose(temp)
temp3 = np.concatenate((temp2[:ny//2][:], temp2[(ny//2)-1:][:]))

with open('dust_temperature.dat','w+') as f:
    f.write('1\n')                       # Format number
    f.write('%d\n'%(nx*ny*nz))           # Nr of cells
    f.write('1\n')                       # Nr of dust species
    data = temp3
    data.tofile(f, sep='\n', format="%13.6e")
    f.write('\n')
#
# Dust opacity control file
#
with open('dustopac.inp','w+') as f:
    f.write('2               Format number of this file\n')
    f.write('1               Nr of dust species\n')
    f.write('============================================================================\n')
    f.write('1               Way in which this dust species is read\n')
    f.write('0               0=Thermal grain\n')
    f.write('silicate        Extension of name of dustkappa_***.inp file\n')
    f.write('----------------------------------------------------------------------------\n')
#
# Write the radmc3d.inp control file
#
with open('radmc3d.inp','w+') as f:
    f.write('nphot = %d\n'%(nphot))
    f.write('scattering_mode_max = 0\n')   # Put this to 1 for isotropic scattering
