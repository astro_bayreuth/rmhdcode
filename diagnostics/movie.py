# -*- coding: utf-8 -*-
"""
Created on Oct 20, 2021

@author: Benjamin Schobert
"""

#########################
#CREATE A SERIES OF PNGs
#########################

import json
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
#FOR LATEX FONT
plt.rc('text', usetex=True)

try:
    os.mkdir("./movie")
except OSError as error:
    print(error)
    print("directory /plots already exits, will be written into")

with open("input_rmhd.json", "r") as json_file:
    data = json.load(json_file)
    
nat_const = data['Natural constants']
kb = nat_const['Boltzmann constant']

units = data['Units']
atomic_unit = units['Atomic unit']

gas_param = data['Gas parameter']
mol_weight = gas_param['Molecular weight']
    
grid_data = data['Grid parameter']
n_radius = grid_data['N_radius']
n_theta = grid_data['N_theta']

boxsize = data['Boxsize']
r_min = boxsize['Minimum radius']
r_max = boxsize['Maximum radius']
theta_min = boxsize['Minimum theta']
theta_max = boxsize['Maximum theta']

simulation_param = data['Simulation parameter']
n_step_1 = simulation_param['Number of steps 1']
n_step_2 = simulation_param['Number of steps 2']
n_step_3 = simulation_param['Number of steps 3']
n_step_4 = simulation_param['Number of steps 4']
n_step_5 = simulation_param['Number of steps 5']
n_step_6 = simulation_param['Number of steps 6']
n_step_7 = simulation_param['Number of steps 7']
n_step_8 = simulation_param['Number of steps 8']
n_step_9 = simulation_param['Number of steps 9']
n_step_10 = simulation_param['Number of steps 10']

n_step = n_step_1 + n_step_2 + n_step_3 + n_step_4 + n_step_5 + n_step_6 + n_step_7 + n_step_8 + n_step_9 + n_step_10
dr=(r_max - r_min)/(n_radius);

rad = np.loadtxt("rad_au")
theta = np.loadtxt("theta")

radlog = np.logspace(np.log10(r_min+dr/2),np.log10(r_max-dr/2),n_radius)
radlin = np.linspace(np.log10(r_min+dr/2),np.log10(r_max-dr/2),n_radius)

for k in range(n_step):

    dens = np.loadtxt(str(k+1) + 'dens.dat')
    #temp = np.loadtxt(str(n_step) + 'temp.dat')
    #Er   = np.loadtxt(str(n_step) + 'Er.dat')
    f_d2g = np.loadtxt(str(k+1) + 'f_d2g.dat')
    
    #tempint = temp
    #Erint = Er
    densint = dens
    fint = f_d2g
    
    for i in range(n_theta):
        #tempint[:,i] = np.interp(radlog, rad, temp[:,i])
        #Erint[:,i] = np.interp(radlog, rad, Er[:,i])
        densint[:,i] = np.interp(radlog, rad, dens[:,i])
        fint[:,i] = np.interp(radlog, rad, f_d2g[:,i])
        
    dustdensint = np.multiply(fint, densint)
    
    midindex = (n_theta+1)/2
    midindex = int(midindex)
    #temp_ev = 2000*np.power(dens[:,midindex]/1000, 0.0195)		#fitting model of Isella & Natta 2005 for the evaporation temperature of dust
    
    #pres = np.multiply(dens,temp)*(kb/(mol_weight*atomic_unit))
    
    #CREATE THE MAIN PLOTS
    #####################
    
    
    fig1 = plt.figure()
    ax1 = fig1.gca()
    im1 = ax1.imshow(np.log(dustdensint.transpose()*1e-3), cmap=cm.jet, extent=[np.log10(r_min+dr/2),np.log10(r_max-dr/2), theta_min, theta_max])
    ax1.set_xlabel('Radius [AU]')
    ax1.set_ylabel("$\\theta - \pi/2$ [rad]")
    xtick=[0.2, 0.3, 0.4, 0.5, 0.7, 1.0, 2.0, 3.0, 4.0];
    xtick2 = np.log10(xtick)
    ax1.set_xticks(xtick2)
    xticklabel=["0.2","0.3","0.4","0.5","0.7","1.0","2.0","3.0","4.0"]
    ax1.set_xticklabels(xticklabel)
    ytick=[np.pi/2-0.15, np.pi/2-0.1, np.pi/2-0.05, np.pi/2, np.pi/2+0.05, np.pi/2+0.1, np.pi/2+0.15]
    ax1.set_yticks(ytick)
    yticklabel=['$-0.15$','$-0.10$','$-0.05$','$0$','$+0.05$','$+0.10$','$+0.15$']
    ax1.set_yticklabels(yticklabel)
    ax_insert = inset_axes(ax1, width='3%', height='100%', loc='lower left', bbox_to_anchor=(1.015, 0.0, 1, 1), bbox_transform=ax1.transAxes, borderpad=0)
    cbar1 = plt.colorbar(im1, cax=ax_insert)
    cbar1.ax.set_ylabel(r"log($\rho_{dust}$) [g/cm$^3$]")
    
    #plt.savefig("plots/out.png",bbox_inches='tight')
    #plt.savefig("plots/out.eps",bbox_inches='tight')  # Saving plot to eps file
    plt.savefig("movie/"+ str(k+1) +"dustdens.png",bbox_inches='tight', dpi=500)
    
    plt.close(fig1)



print("program done")


