n_size = 1000;              #size of the image file
size_au = 100;                #au size of image

%diskAU = 1;
%diskAUmax = 4
%diskRadius = n_size*diskAU/size_au;           #artifical disk radius
%diskRadiusMax = n_size*diskAUmax/size_au;

A =  ones(n_size, n_size);        # image space matrix (of intensities)

x = -n_size/2 + 0.5:1:+n_size/2 - 0.5;         # coordinates in gridpoints (maybe later au?)
y = -n_size/2 + 0.5:1:+n_size/2 - 0.5;
%x = 1:n_size;
%y = 1:n_size;

[xx, yy] = meshgrid(x,y);

#reference files
vis3ref = csvread ('3umRef.dat');
vis9ref = csvread ('9umRef.dat');

# example data

%A = (1/(2*pi*50))*exp(-(xx.**2 + yy.**2)/(2*50.0));         # gaussian
%A = exp(-(4*xx.**2 + yy.**2)/100.0);                        # elliptic gaussian
%A3 = (xx.**2 + yy.**2) < diskRadius**2;                     #disk
%A3 = 1./((sqrt(xx.**2 + yy.**2)-diskRadius).**4+1);            #r^-4 radiation dependency
%A3((xx.**2 + yy.**2) > diskRadiusMax**2) =0;
%A9 = A3;


    
# radmc3d images

A = dlmread("image.out",'\n',7,0);
A3 = A(1:1000000);
A9 = A(1000001: 2000000);

%A9 = load("image9.out");
A9 = reshape(A9, n_size, n_size);
A9 = A9';

%A3 = load("image3.out");
A3 = reshape(A3, n_size, n_size);
A3 = A3';


#shift for fft

%Ashift1 = vertcat(A(251:n_size,:),A(1:250,:));
%Ashift2 = horzcat(Ashift1(:,251:n_size),Ashift1(:,1:250));
A3shift = fftshift(A3);
A9shift = fftshift(A9);

#Fast fourier transform 2d

B3 = fft2(A3shift);
B9 = fft2(A9shift);
%B = fft2(A);

#backshift after fft

%Bshift1 = vertcat(B(251:n_size,:),B(1:250,:));
%Bshift2 = horzcat(Bshift1(:,251:n_size),Bshift1(:,1:250));
B3shift = ifftshift(B3);
B9shift = ifftshift(B9);


#absolute value and real part

B3abs = abs(B3shift);
B3real = real(B3shift);
B3ima = imag(B3shift);
B9abs = abs(B9shift);
B9real = real(B9shift);
B9ima = imag(B9shift);

#normalize visibility (will be done later)


%B3absNorm = B3abs ./ max(max(B3abs));
%B3realNorm = B3real ./ max(max(B3abs));
%B9absNorm = B9abs ./ max(max(B9abs));
%B9realNorm = B9real ./ max(max(B9abs));

B3absNorm = B3abs;
B9absNorm = B9abs;

# now calulate Breal( sqrt(u^2 + v^2)) the radial function in frequency space

s = 0:0.5:n_size/2 - 0.5;
theta = 0:(2*pi)/n_size: 2*pi- (2*pi)/n_size;
uu = s.*cos(theta');
vv = s.*sin(theta');

%B3polar = interp2(xx,yy,B3realNorm, uu, vv, "pchip"); # 2d frequency space in polar coordinates
B3polar2 = interp2(xx,yy,B3absNorm, uu, vv, "pchip"); # 2d frequency space in polar coordinates
%B9polar = interp2(xx,yy,B9realNorm, uu, vv, "pchip"); # 2d frequency space in polar coordinates
B9polar2 = interp2(xx,yy,B9absNorm, uu, vv, "pchip"); # 2d frequency space in polar coordinates

#average over all angles 

%B3rad = sum(B3polar)/n_size;
B3rad2 = sum(B3polar2)/n_size;
%B9rad = sum(B9polar)/n_size;
B9rad2 = sum(B9polar2)/n_size;

#normalize
B3rad2 = B3rad2./ B3rad2(1);
B9rad2 = B9rad2./ B9rad2(1);

#return one visibility value for a specific spacial frequency sWanted
%sWanted = 13;
%vis = interp1(s, Brad, sWanted)


#x axis for visibility

%x_au = x *(2/n_size)*size_au/2;
%x_rad = x_au ./ (154*206265);
%s_over_rad = 1./x_rad;
%s_MlambdaAux = s_over_rad./1e6;
%s_Mlambda = 0:s_MlambdaAux(n_size)/n_size:s_MlambdaAux(n_size)-s_MlambdaAux(n_size)/n_size;

x_au = size_au;
x_rad = x_au ./ (154.03*206265);
x_rad_pixel = x_rad/n_size;
k_abtast = 1/x_rad_pixel;
%k_abtast = 1/x_rad;
%k_max = k_abtast/2;
k_max = k_abtast/(2*2);
k_max_Mlambda = k_max/1e6;
s_Mlambda = 0:k_max_Mlambda/n_size:k_max_Mlambda-k_max_Mlambda/n_size;

#plot the image

raOffsetAU = -size_au/2+size_au/(2*n_size):size_au/n_size :size_au/2-size_au/(2*n_size);
raOffset = raOffsetAU * 6.49;

imagesc(raOffset, raOffset, log(A3));
colorbar();
xlabel("RA offset (mas)");
ylabel("Dec offset (mas)");
print -dpng -color  image3log.png

imagesc(raOffset, raOffset,log(A9));
colorbar();
xlabel("RA offset (mas)");
ylabel("Dec offset (mas)");
print -dpng -color  image9log.png

imagesc(raOffset, raOffset, A3);
colorbar();
xlabel("RA offset (mas)");
ylabel("Dec offset (mas)");
print -dpng -color  image3.png

imagesc(raOffset, raOffset,A9);
colorbar();
xlabel("RA offset (mas)");
ylabel("Dec offset (mas)");
print -dpng -color  image9.png

#plot the visibility

plot(s_Mlambda, B3rad2, s_Mlambda, B9rad2, vis3ref(:,1), vis3ref(:,2), 'x', vis9ref(:,1), vis9ref(:,2), 'x')
xlim([0 50]);
legend("3 micro", "9 micro", "3 micro Varga", "9 micro Varga")
xlabel("B eff in M lambda");
ylabel("visibility");
print -dpng -color  visibility.png

#tests for correctness

%utest = -0.5+1/1000: 1/n_size : 0.5-1/1000;
%utest2 = 0: 1/1000 : 0.5-1/1000;
%expectation = exp(-2*(pi**2)*50*utest.**2);      # gaussian
%expectation = sqrt(1000)*besselj(1,2*pi*sqrt(1000)*utest)./utest;
%expectation2 = sqrt(1000)*besselj(1,2*pi*sqrt(1000)*utest2)./utest2;
%plot(utest, Breal(251,:), utest, expectation)
%legend()
%BpolarTest = interp2(xx,yy,Breal, uu, vv); # 2d frequency space in polar coordinates
%BradTest = sum(BpolarTest)/n_size;
%plot(utest2, BradTest, utest2, expectation2)
%legend()

#save 

save -ascii 'B3rad2.dat' B3rad2
save -ascii 'B9rad2.dat' B9rad2
save -ascii 's_Mlambda.dat' s_Mlambda
