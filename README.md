# README #


### What is this repository for? ###

* This is a work in progress for expanding and improving the radiation code.
* If you want to use a code like this, you should for now use the radiation code found in the radiation_code repository.

### How do I get set up? ###

* make all
* ./run

### Contribution guidelines ###

* This code is open source for any academical use.
* If you use this code in your publcation you should cite B. N. Schobert, A. G. Peeters & F. Rath, 2019, ApJ, 881, 56.

### Who do I talk to? ###

* Benjamin Schobert
* benjamin.schobert@uni-bayreuth.de
