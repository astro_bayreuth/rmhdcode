 
/*	The following program code can be used to simulate 
 *  the evolution of the inner rim of a proto-planetary disk.
 *
 * 
 *	#################################################################################################################################
 */



// HEADER (INCLUDE ALL REQUIRED LIBRARIES)


#include <iostream>                                                         // Include standard input-/output-stream
#include <fstream>
#include <sstream>
#include <iomanip>
#include <include/cjson/cJSON.h> 											// home/btpp/btp00000/shared/source_me_to_have_them_all.sh has to be added to .bashrc
#include "umfpack.h"                                                        // for inverting the matrix
#include <chrono>                                                           // for time measurement
#include <vector>
#include <math.h>                                                           // for power, sqrt, log functions
#include <algorithm>                                                        // for find function

using namespace std;
using namespace std::chrono;

// DEFINING CONSTANTS

#define PI 3.14159265358979323846264338327950
#define DEFAULT_INPUT_FILENAME "input_rmhd.json"



// DECLARING GLOBAL VARIABLES (INPUT VIA JSON)

double kb;                                                                  // Boltzmann constant
double sigma;                                                               // Stefan-Boltzmann constant
double G;                                                                   // Gravitational constant
double c0;                                                                  // Vacuum speed of light
double Na;                                                                  // Avogadro's constant

double atomic_unit;                                                         // atomic unit in kg
double year;                                                                // year in seconds
double au;                                                                  // astronomic unit in meters

double m_solar;                                                             // mass of the sun
double r_solar;                                                             // radius of the sun
double l_solar;                                                             // luminosity of the sun

int n_radius;                                                               // radial gridsize
int n_theta;                                                                // polar gridsize !caution: n_theta needs to be odd

double r_min;                                                               // inner radial boundary in au
double r_max;                                                               // outer radial boundary in au
double theta_min;                                                           // lower polar boundary in rad
double theta_max;                                                           // upper polar boundary in rad

double mol_weight;                                                          // mean molecular weight of the gas molecules
double adia_gamma;                                                          // adiabatic index
double eps;                                                                 // ratio of Planck mean opacities kappa_dust_rim/kappa_dust_star !at the moment not used anywhere
double f_d2g_0;                                                             // reference ratio of dust to gas density
double f_d2g_min;                                                           // minimum dust to gas ratio
double dustTempRange;                                                       // temperature range for the step in the dust to gas ratio in K
double k_dust_star;                                                         // dust opacity for t_star = 10000 K in m^2/kg
double k_dust_rim;                                                          // dust opacity for t_rim = 1350 K in m^2/kg
double k_gas;                                                               // gas opacity in m^2/kg
double Pr;                                                                  // Prandtl number
double q_tau;                                                               // factor for tau_0, normally 1
double mag_trunc;                                                           // factor of stellar radii, where the magnetoshpere is truncated

double r_star;                                                              // radius of the star
double t_star;                                                              // temperature of the star
double m_star;                                                              // mass of the star
double m_dot;                                                               // accretion rate in solar masses per year
double l_star;                                                              // luminosity of the star !at the moment not used anywhere

double alpha_in;                                                            // inner value of the dimensionless alpha-viscosity parameter
double alpha_out;                                                           // outer value of the dimensionless alpha-viscosity parameter
double t_mri;                                                               // temperature at which magneto-rotational instability sets in
double delta_t;                                                             // temperature range for the step in alpha in K

bool visc_heat;                                                             // boolean for viscous heating through accretion
bool heat_cond;                                                             // boolean for heat conduction in the disk
bool dustStep;                                                              // boolean for step in f_d2g function
bool dustTauDep;                                                            // boolean for tau dependency in f_d2g function
bool dustDiff;                                                              // boolean for dust diffusion
int diffusionStart;                                                         // number of the timestep at which diffusion is initiated
double dustDiffTime;                                                        // typical dust diffusion time in seconds
bool selfGravity;                                                           // boolean for self gravity
bool testMode;                                                              // boolean for test mode (turns energy evolution off)
int init_mode;                                                              // set initialization mode for the surface density profile: 'const' or 'accretion'
                                                                            // 1 = const: everywhere surf_dens = 100 gram/cm**2 = 1 000 kg/m**2
                                                                            // 2 = accretion: compute initial surface density profile from alpha viscosity approach surf_dens = m_dot/(3*pi*viscosity)
int alpha_mode;                                                             // set mode for alpha estimation: 'Flock' or 'Kadam'
                                                                            // 1 = 'Flock': uses tanh
                                                                            // 2 = 'Kadam': uses heaviside, for EXor
bool surfEvol;                                                              // boolean for surface density evolution, false = steady state, true = time dependend accretion
int n_time_1;                                                               // number of timesteps in phase 1
int n_time_2;                                                               // number of timesteps in phase 2
int n_time_3;                                                               // number of timesteps in phase 3
int n_time_4;                                                               // number of timesteps in phase 4
int n_time_5;                                                               // number of timesteps in phase 5
int n_time_6;                                                               // number of timesteps in phase 6
int n_time_7;                                                               // number of timesteps in phase 7
int n_time_8;                                                               // number of timesteps in phase 8
int n_time_9;                                                               // number of timesteps in phase 9
int n_time_10;                                                              // number of timesteps in phase 10
double dt_1;                                                                // size of timestep in phase 1
double dt_2;                                                                // size of timestep in phase 2
double dt_3;                                                                // size of timestep in phase 3
double dt_4;                                                                // size of timestep in phase 4
double dt_5;                                                                // size of timestep in phase 5
double dt_6;                                                                // size of timestep in phase 6
double dt_7;                                                                // size of timestep in phase 7
double dt_8;                                                                // size of timestep in phase 8
double dt_9;                                                                // size of timestep in phase 9
double dt_10;                                                               // size of timestep in phase 10
bool dust_var_1;                                                            // boolean for varying f_d2g in phase 1
bool dust_var_2;                                                            // boolean for varying f_d2g in phase 2
bool dust_var_3;                                                            // boolean for varying f_d2g in phase 3
bool dust_var_4;                                                            // boolean for varying f_d2g in phase 4
bool dust_var_5;                                                            // boolean for varying f_d2g in phase 5
bool dust_var_6;                                                            // boolean for varying f_d2g in phase 6
bool dust_var_7;                                                            // boolean for varying f_d2g in phase 7
bool dust_var_8;                                                            // boolean for varying f_d2g in phase 8
bool dust_var_9;                                                            // boolean for varying f_d2g in phase 9
bool dust_var_10;                                                           // boolean for varying f_d2g in phase 10

int order_of_radial_scheme;                                                 // set order of radial integration: 2 or 4
int order_of_theta_scheme;                                                  // set order of theta integration: 2 or 4
int dustlog;                                                                // increase dust to gas ration logarithmically over the first dustlog iterations. 0 means full from beginning
bool renorm;                                                                // boolean for renormalization of integrated density to desired surface density
int restart;                                                                // restart from this iteration number, if 0 start from beginning

double droptol;                                                             // droptol is the drop tolerance for the LU factorization, suggested: 0.0

double total_time;															// Declare system time variable for performance tests

// GLOBAL VARIABLES DERIVED FROM INPUTS

double ar;						                                    //radiation constant
double cV;                                                          //specific heat under constant volume for ideal gas
double cp;					                                        //specific heat under constant pressure
double dtheta;		                                                //theta step size
double dr;			                                                //radial step size
double n_time;			                                            //total number of timesteps

// GLOBAL VARIABLES FOR BOUDARIES

double T_0f;                                                        // inner box boundary temperature
double T_0b;	                                                    // outer box boundary temperature
double Er_0b;                                                       // outer box boundary energy density
vector<double> T_thick(n_radius);                                   // optically thick temperature for flaring disk
double r_trans;                                                     // transition radius
int index_trans;                                                    // index of first element
double h;                                                           // gas scale height for boundary

// DECLARE FUNCTIONS (USING FORWARD DECLARATON, SEE BELOW WHERE THEY ARE DEFINED FOR FURTHER EXPLANATIONS)

void write2d(string const filename, vector<double> & vec, int n_rad, int n_the);
void read2d(string const filename, vector<double> & vec, int n_rad, int n_the);
double get_radius(int i);
double get_theta(int j);
int get_midplane_index();
vector<double> set_temp_init(vector<double> & rad);
vector<double> set_Er_init(vector<double> & temp);
vector<double> get_csound(vector<double> & temp);
vector<double> get_omega(vector<double> & radial_grid);
vector<double> get_nu1(vector<double> & temp, vector<double> & csound, vector<double> & omega);
vector<double> get_nu2(vector<double> & temp, vector<double> & csound, vector<double> & omega, vector<double> & surf_dens);
vector<double> get_surf_dens1(vector<double> & temp, vector<double> & nu, vector<double> & rad);
void get_surf_dens2(vector<double> & surf_dens, vector<double> & v_radial, vector<double> & v_azimuthal, double & Sigma_sink, vector<double> radial_grid, vector<double> nu, vector<double> dr_pres, vector<double> dr_phi, double dt);
void get_boundaries(vector<double> & tau, vector<double> & f_d2g, vector<double> & radial_grid, vector<double> & Er_low, vector<double> & Er_up, vector<double> & Er_0f, vector<double> & T_low, vector<double> & T_up);
vector<double> interp1(vector<double> & data, bool extrap);
void get_dens(vector<double> & dens, vector<double> & dens_int_t, vector<double> & dens_int_r, vector<double> & temp, vector<double> & T_low, vector<double> & T_up, vector<double> & surf_dens, vector<double> & omega, vector<double> & radial_grid, vector<double> & theta_grid, vector<double> & f_d2g, bool selfGravity);
void get_deriv(vector<double> & res, vector<double> & y, vector<double> & hz, vector<double> & T0, vector<double> & T_l, vector<double> & T_r, vector<double> & mu0, vector<double> & mu_l, vector<double> & mu_r, vector<double> & a, double & b);
void get_deriv(vector<double> & res, vector<double> & y, vector<double> & hz, vector<double> & radial_grid0, vector<double> & radial_grid_l, vector<double> & radial_grid_r, vector<double> & nu0, vector<double> & nu_l, vector<double> & nu_r, vector<double> & dr_pres, vector<double> & dr_phi);
void rk4step(vector<double> & next, vector<double> & y, vector<double> & hz, int eq_num, int vec_length, vector<double> & param1_0, vector<double> & param1_l, vector<double> & param1_r, vector<double> & param2_0, vector<double> & param2_l, vector<double> & param2_r, vector<double> & var_param3, auto & var_param4);
void get_dr_grid(vector<double> & dr_grid, vector<double> grid, int grid_size_r, int grid_size_theta, char method, double val1, double val2);
void get_dr_grid(vector<double> & dr_grid, vector<double> grid, int grid_size_r, int grid_size_theta, char method, vector<double> val1, double val2);
void get_dtheta_grid(vector<double> & dtheta_grid, vector<double> grid, int grid_size_r, int grid_size_theta, char method, vector<double> & val1, vector<double> & val2);
vector<double> get_tau(vector<double> & dens, vector<double> & dens_int_r, vector<double> f_d2g);
vector<double> get_F_ast(vector<double> & tau, vector<double> radial_grid);
vector<double> get_f_d2g(vector<double> & dens, vector<double> & temp, vector<double> & tau);
void get_D_T(vector<double> & D_T_r, vector<double> & D_T_t, vector<double> & dens_int_r, vector<double> & dens_int_t, vector<double> & nu, bool heat_cond);
void get_D_Er(vector<double> & D_Er_r, vector<double> & D_Er_t, vector<double> & dens_int_r, vector<double> & dens_int_t, vector<double> & Er, vector<double> & f_d2g, vector<double> & Er_up, vector<double> & Er_low, vector<double> & Er_0f, vector<double> & radial_grid);
void get_dr_stencil(vector<double> & dr_stencil, int & k_min, int & k_max, int order);
void get_dtheta_stencil(vector<double> & dtheta_stencil, int & k_min, int & k_max, int order);
vector<double> get_lambda(vector<double> & grad_Er, vector<double> & Er, vector<double> & sig_R);
void build_rhs(vector<double> & b, vector<double> & temp, vector<double> & dens, vector<double> & Er, vector<double> & dr_omega, vector<double> & nu, vector<double> & f_d2g, vector<double> & F_ast, vector<double> & D_Er_r, vector<double> & D_Er_t, double dt, bool visc_heat, vector<double> & Er_up, vector<double> & Er_low, vector<double> & Er_0f, vector<double> & radial_grid, vector<double> & polar_grid);
void build_matrix(vector<double> & Ax, vector<double> & D_T_r, vector<double> & D_T_t, vector<double> & D_Er_r, vector<double> & D_Er_t, vector<double> & temp, vector<double> & dens, vector<double> & f_d2g, double dt, vector<double> & radial_grid, vector<double> & polar_grid);
void pre_build_matrix(vector<int> & Ap, vector<int> & Ai);
void pre_build_dust_matrix(vector<int> & Ap2, vector<int> & Ai2);
void get_new_fields(vector<double> & x, vector<double> & temp, vector<double> & Er);
void get_diffusion(vector<double> & dens, vector<double> & f_d2g, vector<double> & nu, vector<double> & radial_grid, vector<double> & polar_grid, vector<int> & Ap2, vector<int> & Ai2, vector<double> & Ax2, int matrix_size2, double Info[], double Control[]);

// JSON PARSING

void parseInputFile(const char *filename)
{
    char *buffer = 0;
    long length;
    FILE *f = fopen(filename, "rb");
    
    if (f) {
        fseek(f, 0, SEEK_END);
        length = ftell (f);
        fseek(f, 0, SEEK_SET);
        buffer = new char[length]; 
        
        if(buffer)
        {
            fread(buffer, 1, length, f);
        }
        fclose(f);   
                
    } else {
        printf("\nFILE '%s' COULD NOT BE READ.\n", filename);
        exit(1);
    }
    printf("\nFILE '%s' SUCCESSFULLY READ.\n\n", filename);
    
    if(buffer)
    {	
        cJSON *root = cJSON_Parse(buffer);
               
        cJSON *group;
             
        cJSON *parameterValue; 
        
        // Natural constants
	
        group = cJSON_GetObjectItem(root, "Natural constants");
        
		parameterValue = cJSON_GetObjectItem(group, "Boltzmann constant");
		if(cJSON_IsNumber(parameterValue)){kb = parameterValue -> valuedouble;}
		else{kb = 1.38064852e-23;}
		
		parameterValue = cJSON_GetObjectItem(group, "Stefan-Boltzmann constant");
		if(cJSON_IsNumber(parameterValue)){sigma = parameterValue -> valuedouble;}
		else{sigma = 5.670367e-8;}
		
		parameterValue = cJSON_GetObjectItem(group, "Gravitational constant");
		if(cJSON_IsNumber(parameterValue)){G = parameterValue -> valuedouble;}
		else{G = 6.67408e-11;}
		
		parameterValue = cJSON_GetObjectItem(group, "Vacuum speed of light");
		if(cJSON_IsNumber(parameterValue)){c0 = parameterValue -> valuedouble;}
		else{c0 = 299792458.0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Avogadro's constant");
		if(cJSON_IsNumber(parameterValue)){Na = parameterValue -> valuedouble;}
		else{Na = 6.022140857e23;}
        
        // Units
	
        group = cJSON_GetObjectItem(root, "Units");
        
		parameterValue = cJSON_GetObjectItem(group, "Atomic unit");
		if(cJSON_IsNumber(parameterValue)){atomic_unit = parameterValue -> valuedouble;}
		else{atomic_unit = 1.660538782e-27;}
		
		parameterValue = cJSON_GetObjectItem(group, "Seconds in a year");
		if(cJSON_IsNumber(parameterValue)){year = parameterValue -> valuedouble;}
		else{year = 31536000;}
        
        parameterValue = cJSON_GetObjectItem(group, "Astronomic unit");
		if(cJSON_IsNumber(parameterValue)){au = parameterValue -> valuedouble;}
		else{au = 149597870700;}
		
		// Solar parameter
	
        group = cJSON_GetObjectItem(root, "Solar parameter");
        
		parameterValue = cJSON_GetObjectItem(group, "Solar mass");
		if(cJSON_IsNumber(parameterValue)){m_solar = parameterValue -> valuedouble;}
		else{m_solar = 1.98855e30;}
		
		parameterValue = cJSON_GetObjectItem(group, "Solar radius");
		if(cJSON_IsNumber(parameterValue)){r_solar = parameterValue -> valuedouble;}
		else{r_solar = 6.96342e8;}
        
        parameterValue = cJSON_GetObjectItem(group, "Solar luminosity");
		if(cJSON_IsNumber(parameterValue)){l_solar = parameterValue -> valuedouble;}
		else{l_solar = 3.828e26;}
        
        // Grid parameter
	
        group = cJSON_GetObjectItem(root, "Grid parameter");
        
		parameterValue = cJSON_GetObjectItem(group, "N_radius");
		if(cJSON_IsNumber(parameterValue)){n_radius = parameterValue -> valueint;}
		else{n_radius = 1280;}
		
		parameterValue = cJSON_GetObjectItem(group, "N_theta");
		if(cJSON_IsNumber(parameterValue)){n_theta = parameterValue -> valueint;}
		else{n_theta = 129;}
		
        // Boxsize
        
        group = cJSON_GetObjectItem(root, "Boxsize");
	
		parameterValue = cJSON_GetObjectItem(group, "Minimum radius");
		if(cJSON_IsNumber(parameterValue)){r_min = parameterValue -> valuedouble;}
		else{r_min = 0.2;}
		
		parameterValue = cJSON_GetObjectItem(group, "Maximum radius");
		if(cJSON_IsNumber(parameterValue)){r_max = parameterValue -> valuedouble;}
		else{r_max = 4;}
		
		parameterValue = cJSON_GetObjectItem(group, "Minimum theta");
		if(cJSON_IsNumber(parameterValue)){theta_min = parameterValue -> valuedouble;}
		else{theta_min = 1.390796327;}
		
		parameterValue = cJSON_GetObjectItem(group, "Maximum theta");
		if(cJSON_IsNumber(parameterValue)){theta_max = parameterValue -> valuedouble;}
		else{theta_max = 1.750796327;}
		
		// Gas parameter
        
        group = cJSON_GetObjectItem(root, "Gas parameter");
	
		parameterValue = cJSON_GetObjectItem(group, "Molecular weight");
		if(cJSON_IsNumber(parameterValue)){mol_weight = parameterValue -> valuedouble;}
		else{mol_weight = 2.353;}
		
		parameterValue = cJSON_GetObjectItem(group, "Adiabatic index");
		if(cJSON_IsNumber(parameterValue)){adia_gamma = parameterValue -> valuedouble;}
		else{adia_gamma = 1.42;}
		
		parameterValue = cJSON_GetObjectItem(group, "Planck opacity ratio");
		if(cJSON_IsNumber(parameterValue)){eps = parameterValue -> valuedouble;}
		else{eps = 0.33333333;}
		
		parameterValue = cJSON_GetObjectItem(group, "Reference dust-to-gas ratio");
		if(cJSON_IsNumber(parameterValue)){f_d2g_0 = parameterValue -> valuedouble;}
		else{f_d2g_0 = 0.01;}
		
		parameterValue = cJSON_GetObjectItem(group, "Minimum dust-to-gas ratio");
		if(cJSON_IsNumber(parameterValue)){f_d2g_min = parameterValue -> valuedouble;}
		else{f_d2g_min = 1e-10;}
		
		parameterValue = cJSON_GetObjectItem(group, "Evaporation temperature range");
		if(cJSON_IsNumber(parameterValue)){dustTempRange = parameterValue -> valuedouble;}
		else{dustTempRange = 100;}
		
		parameterValue = cJSON_GetObjectItem(group, "Stellar dust opacity");
		if(cJSON_IsNumber(parameterValue)){k_dust_star = parameterValue -> valuedouble;}
		else{k_dust_star = 210;}
		
		parameterValue = cJSON_GetObjectItem(group, "Reemitted dust opacity");
		if(cJSON_IsNumber(parameterValue)){k_dust_rim = parameterValue -> valuedouble;}
		else{k_dust_rim = 70;}
		
		parameterValue = cJSON_GetObjectItem(group, "Gas opacity");
		if(cJSON_IsNumber(parameterValue)){k_gas = parameterValue -> valuedouble;}
		else{k_gas = 1e-5;}
		
		parameterValue = cJSON_GetObjectItem(group, "Prandtl number");
		if(cJSON_IsNumber(parameterValue)){Pr = parameterValue -> valuedouble;}
		else{Pr = 0.68;}
		
		parameterValue = cJSON_GetObjectItem(group, "Q_tau");
		if(cJSON_IsNumber(parameterValue)){q_tau = parameterValue -> valuedouble;}
		else{q_tau = 1;}
		
		parameterValue = cJSON_GetObjectItem(group, "Magnetic truncation factor");
		if(cJSON_IsNumber(parameterValue)){mag_trunc = parameterValue -> valuedouble;}
		else{mag_trunc = 3.;}
		
		// Stellar parameter
	
        group = cJSON_GetObjectItem(root, "Stellar parameter");
        
		parameterValue = cJSON_GetObjectItem(group, "Stellar radius");
		if(cJSON_IsNumber(parameterValue)){r_star = parameterValue -> valuedouble;}
		else{r_star = 2.5;}
		
		parameterValue = cJSON_GetObjectItem(group, "Stellar temperature");
		if(cJSON_IsNumber(parameterValue)){t_star = parameterValue -> valuedouble;}
		else{t_star = 10000;}
		
		parameterValue = cJSON_GetObjectItem(group, "Stellar mass");
		if(cJSON_IsNumber(parameterValue)){m_star = parameterValue -> valuedouble;}
		else{m_star = 2.5;}
		
		parameterValue = cJSON_GetObjectItem(group, "Accretion rate");
		if(cJSON_IsNumber(parameterValue)){m_dot = parameterValue -> valuedouble;}
		else{m_dot = 1e-8;}
		
		parameterValue = cJSON_GetObjectItem(group, "Stellar luminosity");
		if(cJSON_IsNumber(parameterValue)){l_star = parameterValue -> valuedouble;}
		else{l_star = 56;}
		
		// Viscosity parameter
	
        group = cJSON_GetObjectItem(root, "Viscosity parameter");
        
		parameterValue = cJSON_GetObjectItem(group, "Alpha inner");
		if(cJSON_IsNumber(parameterValue)){alpha_in = parameterValue -> valuedouble;}
		else{alpha_in = 1.9e-2;}
		
		parameterValue = cJSON_GetObjectItem(group, "Alpha outer");
		if(cJSON_IsNumber(parameterValue)){alpha_out = parameterValue -> valuedouble;}
		else{alpha_out = 1.9e-2;}
		
		parameterValue = cJSON_GetObjectItem(group, "MRI temperature");
		if(cJSON_IsNumber(parameterValue)){t_mri = parameterValue -> valuedouble;}
		else{t_mri = 1000;}
		
		parameterValue = cJSON_GetObjectItem(group, "Delta temperature");
		if(cJSON_IsNumber(parameterValue)){delta_t = parameterValue -> valuedouble;}
		else{delta_t = 25;}
		
        // Simulation parameter
	
        group = cJSON_GetObjectItem(root, "Simulation parameter");
        
        parameterValue = cJSON_GetObjectItem(group, "Viscous heat");
		if(cJSON_IsBool(parameterValue)){visc_heat = cJSON_IsTrue(parameterValue);}
		else{visc_heat = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "Heat conduction");
		if(cJSON_IsBool(parameterValue)){heat_cond = cJSON_IsTrue(parameterValue);}
		else{heat_cond = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust step");
		if(cJSON_IsBool(parameterValue)){dustStep = cJSON_IsTrue(parameterValue);}
		else{dustStep = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust tau dependency");
		if(cJSON_IsBool(parameterValue)){dustTauDep = cJSON_IsTrue(parameterValue);}
		else{dustTauDep = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust diffusion");
		if(cJSON_IsBool(parameterValue)){dustDiff = cJSON_IsTrue(parameterValue);}
		else{dustDiff = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "First diffusive step");
		if(cJSON_IsNumber(parameterValue)){diffusionStart = parameterValue -> valueint;}
		else{diffusionStart = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust diffusion time");
		if(cJSON_IsNumber(parameterValue)){dustDiffTime = parameterValue -> valuedouble;}
		else{dustDiffTime = 1e5;}
		
		parameterValue = cJSON_GetObjectItem(group, "self-gravity");
		if(cJSON_IsBool(parameterValue)){selfGravity = cJSON_IsTrue(parameterValue);}
		else{selfGravity = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "Test mode");
		if(cJSON_IsBool(parameterValue)){testMode = cJSON_IsTrue(parameterValue);}
		else{testMode = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "Surface density mode");
		if(cJSON_IsNumber(parameterValue)){init_mode = parameterValue -> valueint;}
		else{init_mode = 1;}
		
		parameterValue = cJSON_GetObjectItem(group, "Alpha parameter mode");
		if(cJSON_IsNumber(parameterValue)){alpha_mode = parameterValue -> valueint;}
		else{alpha_mode = 1;}
		
		parameterValue = cJSON_GetObjectItem(group, "Surface density evolution");
		if(cJSON_IsBool(parameterValue)){surfEvol = cJSON_IsTrue(parameterValue);}
		else{surfEvol = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "Number of steps 1");
		if(cJSON_IsNumber(parameterValue)){n_time_1 = parameterValue -> valueint;}
		else{n_time_1 = 100;}
		
		parameterValue = cJSON_GetObjectItem(group, "Number of steps 2");
		if(cJSON_IsNumber(parameterValue)){n_time_2 = parameterValue -> valueint;}
		else{n_time_2 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Number of steps 3");
		if(cJSON_IsNumber(parameterValue)){n_time_3 = parameterValue -> valueint;}
		else{n_time_3 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Number of steps 4");
		if(cJSON_IsNumber(parameterValue)){n_time_4 = parameterValue -> valueint;}
		else{n_time_4 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Number of steps 5");
		if(cJSON_IsNumber(parameterValue)){n_time_5 = parameterValue -> valueint;}
		else{n_time_5 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Number of steps 6");
		if(cJSON_IsNumber(parameterValue)){n_time_6 = parameterValue -> valueint;}
		else{n_time_6 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Number of steps 7");
		if(cJSON_IsNumber(parameterValue)){n_time_7 = parameterValue -> valueint;}
		else{n_time_7 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Number of steps 8");
		if(cJSON_IsNumber(parameterValue)){n_time_8 = parameterValue -> valueint;}
		else{n_time_8 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Number of steps 9");
		if(cJSON_IsNumber(parameterValue)){n_time_9 = parameterValue -> valueint;}
		else{n_time_9 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Number of steps 10");
		if(cJSON_IsNumber(parameterValue)){n_time_10 = parameterValue -> valueint;}
		else{n_time_10 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Size of step 1");
		if(cJSON_IsNumber(parameterValue)){dt_1 = parameterValue -> valuedouble;}
		else{dt_1 = 1e5;}
		
		parameterValue = cJSON_GetObjectItem(group, "Size of step 2");
		if(cJSON_IsNumber(parameterValue)){dt_2 = parameterValue -> valuedouble;}
		else{dt_2 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Size of step 3");
		if(cJSON_IsNumber(parameterValue)){dt_3 = parameterValue -> valuedouble;}
		else{dt_3 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Size of step 4");
		if(cJSON_IsNumber(parameterValue)){dt_4 = parameterValue -> valuedouble;}
		else{dt_4 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Size of step 5");
		if(cJSON_IsNumber(parameterValue)){dt_5 = parameterValue -> valuedouble;}
		else{dt_5 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Size of step 6");
		if(cJSON_IsNumber(parameterValue)){dt_6 = parameterValue -> valuedouble;}
		else{dt_6 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Size of step 7");
		if(cJSON_IsNumber(parameterValue)){dt_7 = parameterValue -> valuedouble;}
		else{dt_7 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Size of step 8");
		if(cJSON_IsNumber(parameterValue)){dt_8 = parameterValue -> valuedouble;}
		else{dt_8 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Size of step 9");
		if(cJSON_IsNumber(parameterValue)){dt_9 = parameterValue -> valuedouble;}
		else{dt_9 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Size of step 10");
		if(cJSON_IsNumber(parameterValue)){dt_10 = parameterValue -> valuedouble;}
		else{dt_10 = 0;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust varying 1");
		if(cJSON_IsBool(parameterValue)){dust_var_1 = cJSON_IsTrue(parameterValue);}
		else{dust_var_1 = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust varying 2");
		if(cJSON_IsBool(parameterValue)){dust_var_2 = cJSON_IsTrue(parameterValue);}
		else{dust_var_2 = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust varying 3");
		if(cJSON_IsBool(parameterValue)){dust_var_3 = cJSON_IsTrue(parameterValue);}
		else{dust_var_3 = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust varying 4");
		if(cJSON_IsBool(parameterValue)){dust_var_4 = cJSON_IsTrue(parameterValue);}
		else{dust_var_4 = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust varying 5");
		if(cJSON_IsBool(parameterValue)){dust_var_5 = cJSON_IsTrue(parameterValue);}
		else{dust_var_5 = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust varying 6");
		if(cJSON_IsBool(parameterValue)){dust_var_6 = cJSON_IsTrue(parameterValue);}
		else{dust_var_6 = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust varying 7");
		if(cJSON_IsBool(parameterValue)){dust_var_7 = cJSON_IsTrue(parameterValue);}
		else{dust_var_7 = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust varying 8");
		if(cJSON_IsBool(parameterValue)){dust_var_8 = cJSON_IsTrue(parameterValue);}
		else{dust_var_8 = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust varying 9");
		if(cJSON_IsBool(parameterValue)){dust_var_9 = cJSON_IsTrue(parameterValue);}
		else{dust_var_9 = true;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust varying 10");
		if(cJSON_IsBool(parameterValue)){dust_var_10 = cJSON_IsTrue(parameterValue);}
		else{dust_var_10 = true;}
		
		// Integration parameter
	
        group = cJSON_GetObjectItem(root, "Integration parameter");
        
		parameterValue = cJSON_GetObjectItem(group, "Order of radial scheme");
		if(cJSON_IsNumber(parameterValue)){order_of_radial_scheme = parameterValue -> valueint;}
		else{order_of_radial_scheme = 4;}
		
		parameterValue = cJSON_GetObjectItem(group, "Order of theta scheme");
		if(cJSON_IsNumber(parameterValue)){order_of_theta_scheme = parameterValue -> valueint;}
		else{order_of_theta_scheme = 4;}
		
		parameterValue = cJSON_GetObjectItem(group, "Logarithmic dust increase");
		if(cJSON_IsNumber(parameterValue)){dustlog = parameterValue -> valueint;}
		else{dustlog = 25;}
		
		parameterValue = cJSON_GetObjectItem(group, "Dust renormalization");
		if(cJSON_IsBool(parameterValue)){renorm = cJSON_IsTrue(parameterValue);}
		else{renorm = false;}
		
		parameterValue = cJSON_GetObjectItem(group, "Restart from step");
		if(cJSON_IsNumber(parameterValue)){restart = parameterValue -> valueint;}
		else{restart = 0;}
		
		// Umfpack parameter
		
		group = cJSON_GetObjectItem(root, "Umfpack parameter");
        
        parameterValue = cJSON_GetObjectItem(group, "LU drop tolerance");
		if(cJSON_IsNumber(parameterValue)){droptol = parameterValue -> valuedouble;}
		else{droptol = 0.0;}
		
        cJSON_Delete(root);
        
        delete[] buffer;
    }
    
      // Define constant values (determined out of input file)
      ar = 4*sigma/c0;						                                       //radiation constant
      cV = kb/(mol_weight*atomic_unit*(adia_gamma-1));                             //specific heat under constant volume for ideal gas
      cp = adia_gamma*cV;					                                       //specific heat under constant pressure
      dtheta=(theta_max - theta_min)/(n_theta);		                               //theta step size
      dr=(r_max - r_min)*au/(n_radius);			                                   //radial step size
      n_time = n_time_1 + n_time_2 + n_time_3 + n_time_4 + n_time_5 + 
                n_time_6 + n_time_7 + n_time_8 + n_time_9 + n_time_10;			   //total number of timesteps
    
    return;
}



/* ############################
 * Starting with main function
 * ############################
 */

int main()
{
  
    
  // Start the timer
  auto startTime = high_resolution_clock::now();  
  
  // Read input parameter
  parseInputFile(DEFAULT_INPUT_FILENAME);
  
  // Timestep vector
  vector<double> vdt_1(n_time_1, dt_1);
  vector<double> vdt_2(n_time_2, dt_2);
  vector<double> vdt_3(n_time_3, dt_3);
  vector<double> vdt_4(n_time_4, dt_4);
  vector<double> vdt_5(n_time_5, dt_5);
  vector<double> vdt_6(n_time_6, dt_6);
  vector<double> vdt_7(n_time_7, dt_7);
  vector<double> vdt_8(n_time_8, dt_8);
  vector<double> vdt_9(n_time_9, dt_9);
  vector<double> vdt_10(n_time_10, dt_10);
  vector<double> vdt(vdt_1);
  vdt.insert( vdt.end(), vdt_2.begin(), vdt_2.end() );
  vdt.insert( vdt.end(), vdt_3.begin(), vdt_3.end() );
  vdt.insert( vdt.end(), vdt_4.begin(), vdt_4.end() );
  vdt.insert( vdt.end(), vdt_5.begin(), vdt_5.end() );
  vdt.insert( vdt.end(), vdt_6.begin(), vdt_6.end() );
  vdt.insert( vdt.end(), vdt_7.begin(), vdt_7.end() );
  vdt.insert( vdt.end(), vdt_8.begin(), vdt_8.end() );
  vdt.insert( vdt.end(), vdt_9.begin(), vdt_9.end() );
  vdt.insert( vdt.end(), vdt_10.begin(), vdt_10.end() );
  
  // dust_var boolean vector
  vector<bool> vdust_var_1(n_time_1, dust_var_1);
  vector<bool> vdust_var_2(n_time_2, dust_var_2);
  vector<bool> vdust_var_3(n_time_3, dust_var_3);
  vector<bool> vdust_var_4(n_time_4, dust_var_4);
  vector<bool> vdust_var_5(n_time_5, dust_var_5);
  vector<bool> vdust_var_6(n_time_6, dust_var_6);
  vector<bool> vdust_var_7(n_time_7, dust_var_7);
  vector<bool> vdust_var_8(n_time_8, dust_var_8);
  vector<bool> vdust_var_9(n_time_9, dust_var_9);
  vector<bool> vdust_var_10(n_time_10, dust_var_10);
  vector<bool> vdust_var(vdust_var_1);
  vdust_var.insert( vdust_var.end(), vdust_var_2.begin(), vdust_var_2.end() );
  vdust_var.insert( vdust_var.end(), vdust_var_3.begin(), vdust_var_3.end() );
  vdust_var.insert( vdust_var.end(), vdust_var_4.begin(), vdust_var_4.end() );
  vdust_var.insert( vdust_var.end(), vdust_var_5.begin(), vdust_var_5.end() );
  vdust_var.insert( vdust_var.end(), vdust_var_6.begin(), vdust_var_6.end() );
  vdust_var.insert( vdust_var.end(), vdust_var_7.begin(), vdust_var_7.end() );
  vdust_var.insert( vdust_var.end(), vdust_var_8.begin(), vdust_var_8.end() );
  vdust_var.insert( vdust_var.end(), vdust_var_9.begin(), vdust_var_9.end() );
  vdust_var.insert( vdust_var.end(), vdust_var_10.begin(), vdust_var_10.end() );
  
  // allocate arrays
  vector<double> temp(n_radius * n_theta, 0);
  vector<double> dens(n_radius * n_theta, 0);
  vector<double> dens_int_t(n_radius * (n_theta+1), 0);
  vector<double> dens_int_r((n_radius+1) * n_theta, 0);
  //vector<double> logdens(n_radius * n_theta, 0);
  vector<double> v_azimuthal(n_radius * n_theta, 0);
  vector<double> v_radial(n_radius * n_theta, 0);
  //vector<double> pres(n_radius * n_theta, 0);
  vector<double> dr_pres(n_radius * n_theta, 0);
  vector<double> csound(n_radius * n_theta, 0);
  vector<double> omega(n_radius * n_theta, 0);
  vector<double> dr_omega(n_radius * n_theta, 0);
  vector<double> surf_dens(n_radius, 0);
  double Sigma_sink = 0;
  vector<double> Er(n_radius * n_theta, 0);
  vector<double> nu(n_radius * n_theta, 0);
  vector<double> tau((n_radius+1) * n_theta, 0);
  vector<double> f_d2g(n_radius * n_theta, 0);
  vector<double> F_ast((n_radius+1) * n_theta, 0);
  vector<double> Er_low(n_radius, 0);
  vector<double> Er_up(n_radius, 0);
  //double Er_0f;
  vector<double> Er_0f(n_theta, 0);
  vector<double> T_low(n_radius, 0);
  vector<double> T_up(n_radius, 0);
  vector<double> dr_phi(n_radius, 0);
  vector<double> D_T_r((n_radius+1)*n_theta, 0);
  vector<double> D_T_t(n_radius*(n_theta+1),0);
  vector<double> D_Er_r((n_radius+1)*n_theta, 0);
  vector<double> D_Er_t(n_radius*(n_theta+1),0);
  //double x0 [2 * n_radius * n_theta];           // arrays for umfpack
  vector<double> x(2 * n_radius * n_theta, 0);
  vector<double> b(2 * n_radius * n_theta, 0);
  int matrix_size = 2*n_radius*n_theta;
  int matrix_size2 = n_radius*n_theta;
  vector<int> Ap(2 * n_radius * n_theta +1, 0);
  vector<int> Ap2(n_radius * n_theta +1, 0);
  int n_entries = 4*4 + 2*(2*n_theta - 2)*5 + (2*n_theta*n_radius - 4*n_theta)*6;
  int n_entries2 = 2*3 + 2*(n_theta - 1)*4 + (n_theta*n_radius - 2*n_theta)*5;
  vector<int> Ai(n_entries, 0);
  vector<int> Ai2(n_entries2, 0);
  vector<double> Ax(n_entries, 0);
  vector<double> Ax2(n_entries2, 0);
  //double *null = ( double * ) NULL;             // this is for umfpack, later this will be control inputs
  double Info [UMFPACK_INFO];
  double Control [UMFPACK_CONTROL];
  void *Numeric;
  int status;
  void *Symbolic;
  double dt;
  
  
  // output grid
  vector<double> radial_grid(n_radius, 0);
  vector<double> theta_grid(n_theta, 0);
  vector<double> radial_grid_au(n_radius, 0);
  //int mi = get_midplane_index();
  
  for(int i=0; i< n_radius; i++){
      radial_grid.at(i) = get_radius(i);
  }
  for(int j=0; j< n_theta; j++){
      theta_grid.at(j) = get_theta(j);
  }
  for(int i=0; i< n_radius; i++){
      radial_grid_au.at(i) = radial_grid.at(i)/au;
  }
  for(int i=0; i< n_radius; i++){
      dr_phi.at(i) = G*(m_star*m_solar)/pow(radial_grid.at(i),2);
  }
  
  // get the default control parameters
  umfpack_di_defaults (Control);
  Control[14] = droptol;
  
  // print the control parameters
  umfpack_di_report_control (Control);
  
  // prebuild matrix, only needs to be done once
  pre_build_matrix(Ap, Ai);
  
  // pre_build_dust_matrix
  if(dustDiff == true){
    pre_build_dust_matrix(Ap2, Ai2);
  }
  
  stringstream radfilename;
  radfilename << "rad";
  write2d(radfilename.str(), radial_grid, n_radius, 1);
  
  stringstream thetafilename;
  thetafilename << "theta";
  write2d(thetafilename.str(), theta_grid, 1, n_theta);
  
  stringstream radaufilename;
  radaufilename << "rad_au";
  write2d(radaufilename.str(), radial_grid_au, n_radius, 1);
  
  //fixed boundary constants
  
  T_0f = pow(r_star*r_solar/(2.*radial_grid.at(0)),0.5)*t_star;		                  // inner box boundary temperature
  T_0b = pow(r_star*r_solar/(2.*radial_grid.at(n_radius-1)),0.5)*t_star;	          // outer box boundary temperature

  Er_0b = ar*pow(259.4,4);						                                      // outer box boundary energy density
  for(int l=0; l< n_radius; l++){
      T_thick.push_back( 550/pow(radial_grid_au.at(l),3./7));                         // optically thick temperature for flaring disk
  }
  
  double r_bc = 0.46*au * pow((k_dust_star/k_dust_rim)/3,0.5) * pow(t_star/1e4,2)*(r_star/2.5);
  double Gamma = 3.1* pow(3.6/4.8,8./7) * pow(l_star/56,2./7) * pow(m_star/2.5,-0.5);
  r_trans = pow(1+Gamma,0.5) *r_bc;							                          // transition radius
  
  vector<bool> trans_area(n_radius, false);                                           // transition area
  for(int i=0; i< n_radius; i++){
      trans_area.at(i) = radial_grid.at(i) >= r_trans;
  }
  
  auto element = find(trans_area.begin(), trans_area.end(), true);
  index_trans = element - trans_area.begin();                                         // index of first element
  h = 0.04*r_trans;								                                      // gas scale height
  
  // initialize profiles
  
  // initialize temperature and radiation energy
	if(restart == 0){
		temp = set_temp_init(radial_grid);
		Er = set_Er_init(temp);
    }else{
        stringstream taufilename;
        taufilename << restart << "tau.dat";
		read2d(taufilename.str(), tau, n_radius+1, n_theta);
        stringstream tempfilename;
        tempfilename << restart << "temp.dat";
		read2d(tempfilename.str(), temp, n_radius, n_theta);
        stringstream Erfilename;
        Erfilename << restart << "Er.dat";
		read2d(Erfilename.str(), Er, n_radius, n_theta);
        stringstream f_d2gfilename;
        f_d2gfilename << restart << "f_d2g.dat";
		read2d(f_d2gfilename.str(), f_d2g, n_radius, n_theta);
    }
  
  csound = get_csound(temp);
  omega = get_omega(radial_grid);
  for(int i =0; i<n_radius; i++){
      v_azimuthal.at(i) = omega.at(n_theta*i) * radial_grid.at(i);
  }
  get_dr_grid(dr_omega, omega, n_radius, n_theta, 'l', 0, 0);
  nu = get_nu1(temp, csound, omega);
  surf_dens = get_surf_dens1(temp, nu, radial_grid);
  
  stringstream tempInitfilename;
  tempInitfilename << "temp_init.dat";
  write2d(tempInitfilename.str(), temp, n_radius, n_theta);
  
  stringstream ErInitfilename;
  ErInitfilename << "Er_init.dat";
  write2d(ErInitfilename.str(), Er, n_radius, n_theta);
  
  stringstream omegafilename;
  omegafilename << "omega.dat";
  write2d(omegafilename.str(), omega, n_radius, n_theta);
  
  stringstream nuInitfilename;
  nuInitfilename << "nu_init.dat";
  write2d(nuInitfilename.str(), nu, n_radius, n_theta);
  
  stringstream csoundInitfilename;
  csoundInitfilename << "csound_init.dat";
  write2d(csoundInitfilename.str(), csound, n_radius, n_theta);
  
  stringstream surfdensInitfilename;
  surfdensInitfilename << "surf_dens_init.dat";
  write2d(surfdensInitfilename.str(), surf_dens, n_radius, 1);
  
  stringstream dromegefilename;
  dromegefilename << "dr_omega.dat";
  write2d(dromegefilename.str(), dr_omega, n_radius, n_theta);
  
  
  
  int start = restart +1;
  
  
  for(int i = start; i<= n_time; i++)
  {
      /*-----------------------------------------------------------------------------------------------------------------------------------
      * perform integration of hydrostatic balance equations
      * -----------------------------------------------------------------------------------------------------------------------------------
      */
      
      
      dt = vdt.at(i-1);
      
      // get boundary conditions for T and Er
      get_boundaries(tau, f_d2g, radial_grid, Er_low, Er_up, Er_0f, T_low, T_up);
      
      
      // get surface density
      if(surfEvol){
            vector<double> dr_temp(n_radius*n_theta, 0);
            vector<double> dr_dens(n_radius*n_theta, 0);
            get_dr_grid(dr_temp, temp, n_radius, n_theta, 'c', T_0f, T_0b);
            get_dr_grid(dr_dens, dens, n_radius, n_theta, 'l', 0, 0);
            for(int i= 0; i< n_radius*n_theta; i++){
                dr_pres.at(i) = (dr_temp.at(i)*dens.at(i)+temp.at(i)*dr_dens.at(i))*kb/(atomic_unit*mol_weight);
            }
            
            get_surf_dens2(surf_dens, v_radial, v_azimuthal, Sigma_sink, radial_grid, nu, dr_pres, dr_phi, dt);
      }else{
            surf_dens = get_surf_dens1(temp, nu, radial_grid);
      }
      
      // density through vertical equilibrium
      get_dens(dens, dens_int_t, dens_int_r, temp, T_low, T_up, surf_dens, omega, radial_grid, theta_grid, f_d2g, selfGravity);
      
      csound = get_csound(temp);
      switch(alpha_mode)
      {
        case 1:
            nu = get_nu1(temp, csound, omega);
            break;
        case 2:
            nu = get_nu2(temp, csound, omega, surf_dens);
            break;
        default:
            cout << "No valid input for alpha mode, mode 1 is chosen." << endl;
            nu = get_nu1(temp, csound, omega);
            break;
      }
      
      // get optical depth and radiation flux
      tau = get_tau(dens, dens_int_r, f_d2g);
      F_ast = get_F_ast(tau, radial_grid);
      
      // diffusion constants for temperature and radiation energy
      get_D_T(D_T_r, D_T_t, dens_int_r, dens_int_t, nu, heat_cond);
      get_D_Er(D_Er_r, D_Er_t, dens_int_r, dens_int_t, Er, f_d2g, Er_up, Er_low, Er_0f, radial_grid);
      
      
      // perform matrix inversion
      if(!testMode){
        build_rhs(b, temp, dens, Er, dr_omega, nu, f_d2g, F_ast, D_Er_r, D_Er_t, dt, visc_heat, Er_up, Er_low, Er_0f, radial_grid, theta_grid);
        build_matrix(Ax, D_T_r, D_T_t, D_Er_r, D_Er_t, temp, dens, f_d2g, dt, radial_grid, theta_grid);
        
        
        
        //status = umfpack_di_report_matrix ( matrix_size, matrix_size, &Ap[0], &Ai[0], &Ax[0], 1, Control);
        //cout << "status matrix: " << status << endl;
        //umfpack_di_report_status (Control, status) ;

        //
        //  Carry out the symbolic factorization.
        //
        status = umfpack_di_symbolic ( matrix_size, matrix_size, &Ap[0], &Ai[0], &Ax[0], &Symbolic, Control, Info );
        //cout << "status 1: " << status << endl;
        umfpack_di_report_status (Control, status) ;
        //
        //  Use the symbolic factorization to carry out the numeric factorization.
        //
        status = umfpack_di_numeric (&Ap[0], &Ai[0], &Ax[0], Symbolic, &Numeric, Control, Info );
        //cout << "status 2: " << status << endl;
        umfpack_di_report_status (Control, status) ;
        //
        //  Free the memory associated with the symbolic factorization.
        //
        umfpack_di_free_symbolic ( &Symbolic );
        //
        //  Solve the linear system.
        //
        status = umfpack_di_solve ( UMFPACK_A, &Ap[0], &Ai[0], &Ax[0], &x[0], &b[0], Numeric, Control, Info );
        //cout << "status 3: " << status << endl;
        umfpack_di_report_status (Control, status) ;
        //
        //  Free the memory associated with the numeric factorization.
        //
        umfpack_di_free_numeric ( &Numeric );
        //
        //  Rearrange x into temp and Er
        //
        get_new_fields(x, temp, Er);
      }
      
      // dust-to-gas ratio
      if(vdust_var.at(i-1)){
        f_d2g = get_f_d2g(dens, temp, tau);
        //cout << "dust varies" << endl;
        if(dustDiff && i>= diffusionStart){             //perform diffusive step on dust and gas density and calculate f_d2g from these
            get_diffusion(dens, f_d2g, nu, radial_grid, theta_grid, Ap2, Ai2, Ax2, matrix_size2, Info, Control);
        }
      }
      
      // switching on dust logarithmically
      if(i< dustlog){
          double prefac_exp = -5 + (5./(dustlog-1))*(i-1); 
          double prefac = pow(10, prefac_exp);
          for(int i = 0; i< n_radius*n_theta; i++){
              f_d2g.at(i) = f_d2g.at(i)*prefac;
          }
      }
      
      // write outputs
      
      if(i%20 == 0){
        stringstream tempfilename;
        tempfilename << i << "temp.dat";
        write2d(tempfilename.str(), temp, n_radius, n_theta);
        
        stringstream Erfilename;
        Erfilename << i << "Er.dat";
        write2d(Erfilename.str(), Er, n_radius, n_theta);
        
        stringstream densfilename;
        densfilename << i << "dens.dat";
        write2d(densfilename.str(), dens, n_radius, n_theta);
        
        stringstream fd2gfilename;
        fd2gfilename << i << "f_d2g.dat";
        write2d(fd2gfilename.str(), f_d2g, n_radius, n_theta);
        
        stringstream taufilename;
        taufilename << i << "tau.dat";
        write2d(taufilename.str(), tau, n_radius+1, n_theta);
        
        stringstream Fastfilename;
        Fastfilename << i << "F_ast.dat";
        write2d(Fastfilename.str(), F_ast, n_radius, n_theta);
        
        stringstream surfdensfilename;
        surfdensfilename << i << "surf_dens.dat";
        write2d(surfdensfilename.str(), surf_dens, n_radius, 1);
      
      }
      
      /*
      
      stringstream tlowfilename;
      tlowfilename << i << "T_low.dat";
      write2d(tlowfilename.str(), T_low, n_radius, 1);
      
      stringstream tupfilename;
      tupfilename << i << "T_up.dat";
      write2d(tupfilename.str(), T_up, n_radius, 1);
      
      stringstream afilename;
      afilename << i << "D_T_r.dat";
      write2d(afilename.str(), D_T_r, n_radius+1, n_theta);
      
      stringstream bfilename;
      bfilename << i << "D_Er_r.dat";
      write2d(bfilename.str(), D_Er_r, n_radius+1, n_theta);
      
      stringstream cfilename;
      cfilename << i << "D_T_t.dat";
      write2d(cfilename.str(), D_T_t, n_radius, n_theta+1);
      
      stringstream dfilename;
      dfilename << i << "D_Er_t.dat";
      write2d(dfilename.str(), D_Er_t, n_radius, n_theta+1);
      
      stringstream nufilename;
      nufilename << i << "nu.dat";
      write2d(nufilename.str(), nu, n_radius, n_theta);
      
      */
      
      cout << "Step: "<< i << endl;
  }
  
  
  // Stop the timer
  auto end = high_resolution_clock::now();
  total_time = duration_cast<milliseconds>(end-startTime).count();
  
  cout << "Time total: " << total_time/(1000.0) << " s" << endl;
  
  // End program
  cout << "Program successfully completed." << endl;
  return 0;
}

/* ##########################################################################
 * End of main function, in the following all required functions are defined 
 * ##########################################################################
 */




// FUNCTION THAT WRITES THE VALUES FOR EACH GRID POINT (i,j)
void write2d(string const filename, vector<double> & vec, int n_rad, int n_the)
{
  fstream output;
  output << setprecision(9);
  output.open (filename.c_str(), ios::out);
  for(int i = 0; i < n_rad; i++)
  {
    for(int j = 0; j < n_the; j++)
    {
      output << setw(14) << vec.at(i*n_the + j) << ' ';
    }
    output << endl;
  }
  output.close();
}

// FUNCTION THAT READS IN THE VALUES FOR EACH GRID POINT (i,j)
void read2d(string const filename, vector<double> & vec, int n_rad, int n_the)
{
    fstream input;  
    input.open(filename, ios::in);
    string zeile1;
    int i = 0;
    while(getline(input, zeile1)){
      stringstream zeilenpuffer(zeile1);
      for (int j = 0; j < n_the; j++){
        zeilenpuffer >> vec.at(i*n_the + j);
      }
      i++;  
    }
    input.close();
}

// FUNCTON THAT RETURNS RADIUS AT RADIAL POSITION i
double get_radius(int i)
{
	double radius = r_min*au + dr/2 + (i)*dr;
	
	return radius;
}



// FUNCTION THAT RETURNS POLOIDAL ANGLE AT POLOIDAL POSITION j
double get_theta(int j)
{
	double theta = theta_min + dtheta/2 + (j)*dtheta;
    
	return theta;
}



// FUNCTION THAT RETURNS MIDPLANE INDEX j_mid
int get_midplane_index()
{
	int j_mid = (n_theta-1)/2;

	return j_mid;
}

// FUNCTION THAT SETS THE INITIAL TEMPERATURE
vector<double> set_temp_init(vector<double> & rad)
{
    vector<double> temp(n_radius * n_theta, 0);
    for(int i = 0; i < n_radius; i++)
    {
      for(int j = 0; j < n_theta; j++)
      {
        temp.at(i*n_theta + j) = pow(r_star*r_solar/(2*rad.at(i)),0.5)*t_star;
      }
    }
    return temp;
}


// FUNCTION THAT SETS THE INITIAL RADIATION ENERGY DENSITY
vector<double> set_Er_init(vector<double> & temp)
{
    vector<double> Er(n_radius * n_theta, 0);
    for(int i = 0; i < n_radius; i++)
    {
      for(int j = 0; j < n_theta; j++)
      {
        Er.at(i*n_theta + j) = pow(temp.at(i*n_theta + j),4)*ar;
      }
    }
    return Er;
}

// FUNCTION THAT RETURNS THE SPEED OF SOUND
vector<double> get_csound(vector<double> & temp)
{
    vector<double> csound(n_radius * n_theta, 0);
    for(int i = 0; i < n_radius; i++)
    {
      for(int j = 0; j < n_theta; j++)
      {
        csound.at(i*n_theta + j) = sqrt(kb*adia_gamma*temp.at(i*n_theta + j)/(atomic_unit*mol_weight));
      }
    }
	return csound;
}

// FUNCTION THAT RETURNS THE KEPLER ROTATIONAL FREQUENCY
vector<double> get_omega(vector<double> & radial_grid)
{
    vector<double> omega(n_radius * n_theta, 0);
    for(int i = 0; i < n_radius; i++)
    {
      for(int j = 0; j < n_theta; j++)
      {
        omega.at(i*n_theta + j) = sqrt(G*m_star*m_solar/pow(radial_grid.at(i),3));
      }
    }
	return omega;
}

// FUNCTION THAT RETURNS THE TURBULENT VISCOSITY FOR ALPHA MODE 1
vector<double> get_nu1(vector<double> & temp, vector<double> & csound, vector<double> & omega)
{
    vector<double> nu(n_radius * n_theta, 0);
    for(int i = 0; i < n_radius; i++)
    {
      for(int j = 0; j < n_theta; j++)
      {
        nu.at(i*n_theta + j) = pow(csound.at(i*n_theta + j),2)*((alpha_in - alpha_out)*((1-tanh((t_mri-temp.at(i*n_theta + j))/delta_t))/2)+alpha_out)/omega.at(i*n_theta + j);
      }
    }
	return nu;
}

// FUNCTION THAT RETURNS THE TURBULENT VISCOSITY FOR ALPHA MODE 2
vector<double> get_nu2(vector<double> & temp, vector<double> & csound, vector<double> & omega, vector<double> & surf_dens)
{
    vector<double> nu(n_radius * n_theta, 0);
    vector<double> alpha(n_radius * n_theta, 0);
    
    for(int i = 0; i < n_radius; i++)
    {
        double Sigma_a = 100.0; // set to 100 or 10 g cm⁻² in Kadam paper
        double Sigma_d = 0.0;
        
        if (surf_dens.at(i) > Sigma_a) {
            Sigma_d = surf_dens.at(i) - Sigma_a;
        }
        
        double alpha_a = 0.01;
        
        double T_crit = 1300.0; // set to 1300 or 1500 K in Kadam paper
        double alpha_MRId = 0.0;
        
        if (temp.at(i) > T_crit) { // midplane temperature?
            alpha_MRId = alpha_a;
        }
        
        double alpha_rd = min(0.00001, alpha_a * Sigma_a / Sigma_d);
        
        double alpha_d = alpha_MRId + alpha_rd;
        
        double alpha_eff = (Sigma_a * alpha_a + Sigma_d * alpha_d) / surf_dens.at(i);
            
        for(int j = 0; j < n_theta; j++)
        {
            alpha.at(i*n_theta + j) = alpha_eff;    // alpha code  for bachelor project here
            nu.at(i*n_theta + j) = pow(csound.at(i*n_theta + j),2)*alpha.at(i*n_theta +j)/omega.at(i*n_theta + j); // Shakura-Sunyaev
        }
    }  

	return nu;
}


// FUNCTION THAT RETURNS THE SURFACE DENSITY FOR STEADY STATE
vector<double> get_surf_dens1(vector<double> & temp, vector<double> & nu, vector<double> & rad)
{
    vector<double> surf_dens(n_radius, 1000);
    switch(init_mode)
    {
        case 1:
            break;
        case 2:
            {
            int mi = get_midplane_index();
            for(int i = 0; i < n_radius; i++)
            {
                surf_dens.at(i) = (m_dot*m_solar/year)/(3*PI*nu.at(i*n_theta + mi))*(1.-sqrt((mag_trunc*r_star*r_solar)/rad.at(i)));
            }
            }
            break;
        default:
            cout << "No valid input for surface mode, constant mode is chosen." << endl;
            break;
    }
    
	return surf_dens;
}

// FUNCTION THAT RETURNS THE SURFACE DENSITY WITH EVOLUTION
void get_surf_dens2(vector<double> & surf_dens, vector<double> & v_radial, vector<double> & v_azimuthal, double & Sigma_sink, vector<double> radial_grid, vector<double> nu, vector<double> dr_pres, vector<double> dr_phi, double dt)
{
    

    vector<double> radial_grid0;
    vector<double> radial_grid_l;
    vector<double> radial_grid_r;
    
    vector<double> nu0;
    vector<double> nu_l;
    vector<double> nu_r;
    vector<double> hz;
    vector<double> next;
    vector<double> Sigma_vr;
    vector<double> Sigma_vazi;
    
    radial_grid.push_back(radial_grid.at(n_radius-1)+dr); //outer BC for radial grid
    radial_grid.insert(radial_grid.begin(), radial_grid.at(0)-dr); // inner BC for radial grid
    
    // do all the other BC conditions and put them in ysol
    
    //surf_dens.push_back(0); //outer BC for surface density (free flow)
    //surf_dens.insert(surf_dens.begin(), Sigma_sink); //inner BC for surface density (via sink cell)
    
    //v_azimuthal.push_back(0); // outer BC for azimuthal velocity (?)
    //v_azimuthal.insert(v_azimuthal.begin(), sqrt(G * m_star * m_solar / radial_grid.at(0)));  // inner BC for azimuthal velocity (Keplerian rotation)
    
    //v_radial.push_back(0); // outer BC for radial velocity (?)
    //v_radial.insert(v_radial.begin(), v_radial.at(0)); // inner BC for radial velocity (zero gradient condition)
    
    nu.push_back(0); // outer BC for nu (?)
    nu.insert(nu.begin(), 0); // inner BC for nu (?)
    
    for(int j = 0; j<n_radius; j++){
        radial_grid_l.push_back(radial_grid.at(j));
        radial_grid0.push_back(radial_grid.at(j+1));
        radial_grid_r.push_back(radial_grid.at(j+2));
        
        // fill all the other vectors here in a similar way
        
        nu_l.push_back(nu.at(j));
        nu0.push_back(nu.at(j+1));
        nu_r.push_back(nu.at(j+2));
        
        hz.push_back(dt);   // this will need to be 2 cells longer (n_radius +2);
        Sigma_vr.push_back(surf_dens.at(j)*v_radial.at(j));
        Sigma_vazi.push_back(surf_dens.at(j)*v_azimuthal.at(j));
    }
    vector<double> ysol = surf_dens;

    ysol.insert(ysol.end(), Sigma_vr.begin(), Sigma_vr.end());
    
    ysol.insert(ysol.end(), Sigma_vazi.begin(), Sigma_vazi.end());
    
    rk4step(next, ysol, hz, 3, n_radius+2, radial_grid0, radial_grid_l, radial_grid_r, nu0, nu_l, nu_r, dr_pres, dr_phi);
    
    for(int j = 0; j<n_radius; j++){
        surf_dens.at(j) = next.at(j);
        v_radial.at(j) = next.at(j + 2*n_radius) / surf_dens.at(j);
        v_azimuthal.at(j) = next.at(j + 4*n_radius) / surf_dens.at(j);
    }

    
	return;
}

// FUNCTION THAT CALCULATES THE BOUDARY CONDITIONS
void get_boundaries(vector<double> & tau, vector<double> & f_d2g, vector<double> & radial_grid, vector<double> & Er_low, vector<double> & Er_up, vector<double> & Er_0f, vector<double> & T_low, vector<double> & T_up)
{
    // general boundary conditions
    //int mi = get_midplane_index();
    //vector<double> tau_mid(n_radius+1,0);
    vector<double> tau_up(n_radius+1,0);
    vector<double> tau_low(n_radius+1,0);
    for(int i = 0; i < n_radius+1; i++)
    {
        //tau_mid.at(i) = tau.at(i*n_theta + mi);
        tau_up.at(i) = tau.at((i+1)*n_theta - 1);
        tau_low.at(i) = tau.at(i*n_theta);
    }
	//vector<double> tau_int_r;
	vector<double> tau_up_int_r;
    vector<double> tau_low_int_r;
	//tau_int_r = interp1(tau_mid, false);                                           // interpolates each colum of tau (radial in midplane) linearly, now n_radius long
    tau_up_int_r = interp1(tau_up, false);                                           // interpolates each colum of tau (radial upper box boundary) linearly, now n_radius long
    tau_low_int_r = interp1(tau_low, false);                                           // interpolates each colum of tau (radial lower box boundary) linearly, now n_radius long
	//vector<double> tau_int(n_radius,0);                                             // minimum of tau and 1 in midplane
    vector<double> tau_up_int(n_radius,0);                                             // minimum of tau and 1 upper box bc
    vector<double> tau_low_int(n_radius,0);                                             // minimum of tau and 1 lower box bc
	for(int i = 0; i < n_radius; i++)
    {
        //tau_int.at(i) = min(tau_int_r.at(i),1.);
        tau_up_int.at(i) = min(tau_up_int_r.at(i),1.);
        tau_low_int.at(i) = min(tau_low_int_r.at(i),1.);
    }
	//Er_0f = (1-exp(-tau.at(mi)))*ar*pow(T_0f,4);                                        // inner box boundary energy density
	for(int j = 0; j< n_theta; j++){
        Er_0f.at(j) = (1-exp(-tau.at(j)))*ar*pow(T_0f,4);                                   // inner box boundary energy density
    }
	
	// lower boudary conditions
	
    vector<double> eps_low;
    for(int i = 0; i < n_radius; i++)
    {
        eps_low.push_back((k_gas + f_d2g.at(i*n_theta)*k_dust_rim)/(k_gas + f_d2g.at(i*n_theta)*k_dust_star));           // lower ratio of opacities
        T_low.at(i) = pow(1/eps_low.at(i),0.25)*sqrt(r_star*r_solar/(2*radial_grid.at(i)))*t_star;                   // lower box boundary temperature, optically thin
    }
    
    
	double T_trans = T_low.at(index_trans);                                                                              // tranistion temperature
    for(int i = index_trans; i < n_radius; i++)
    {
        T_low.at(i) = pow(2/PI*(atan((r_trans - radial_grid.at(i))/ h)+ PI/2),0.25)*T_trans;                    // lower box boundary temperature, tranistion
    }
	for(int i = 0; i < n_radius; i++)
    {
        if ((f_d2g.at(i*n_theta) >= 1e-2) && (T_low.at(i) < T_thick.at(i)))                                      // optically thick area
        {
            T_low.at(i) = T_thick.at(i);                                                                       // lower box boundary temperature, optically thick
        }
        Er_low.at(i) = (1-exp(-tau_low_int.at(i)))*ar*pow(T_low.at(i),4);                                                // lower box boundary energy density
    }
	
	// upper boundary conditions
	
	vector<double> eps_up;
    for(int i = 0; i < n_radius; i++)
    {
        eps_up.push_back((k_gas + f_d2g.at((i+1)*n_theta-1)*k_dust_rim)/(k_gas + f_d2g.at((i+1)*n_theta-1)*k_dust_star));           // upper ratio of opacities
        T_up.at(i) = pow(1/eps_up.at(i),0.25)*sqrt(r_star*r_solar/(2*radial_grid.at(i)))*t_star;                   // upper box boundary temperature, optically thin
    }
    
    for(int i = index_trans; i < n_radius; i++)
    {
        T_up.at(i) = pow(2/PI*(atan((r_trans - radial_grid.at(i))/ h)+ PI/2),0.25)*T_trans;                    // upper box boundary temperature, tranistion
    }
	for(int i = 0; i < n_radius; i++)
    {
        if ((f_d2g.at((i+1)*n_theta-1) >= 1e-2) && (T_up.at(i) < T_thick.at(i)))                                      // optically thick area
        {
            T_up.at(i) = T_thick.at(i);                                                                       // upper box boundary temperature, optically thick
        }
        Er_up.at(i) = (1-exp(-tau_up_int.at(i)))*ar*pow(T_up.at(i),4);                                                // upper box boundary energy density
    }
    
    return;
}

// FUNCTION THAT INTERPOLATES LINEAR EQUIDISTANT AND WITH 2 EXTRAPOLATED VALUES EXTRAP IS TRUE
vector<double> interp1(vector<double> & data, bool extrap)
{
    vector<double> output;
    if(extrap){
    output.push_back(data.at(0) - 0.5*(data.at(1) - data.at(0)));
    for(int i = 0; (unsigned) i < data.size()-1; i++)
    {
        output.push_back(0.5*(data.at(i+1) + data.at(i)));
    }
    output.push_back(data.at(data.size()-1) + 0.5*(data.at(data.size()-1) - data.at(data.size()-2)));
    }else{
    for(int i = 0; (unsigned) i < data.size()-1; i++)
    {
        output.push_back(0.5*(data.at(i+1) + data.at(i)));
    }
    }
    return output;
    
}

// FUNCTION THAT CALCULATES THE DENSITY THROUGH VERTICAL EQUILIBRIUM
void get_dens(vector<double> & dens, vector<double> & dens_int_t, vector<double> & dens_int_r, vector<double> & temp, vector<double> & T_low, vector<double> & T_up, vector<double> & surf_dens, vector<double> & omega, vector<double> & radial_grid, vector<double> & theta_grid, vector<double> & f_d2g, bool selfGravity)
{
    vector<double> a;
    double chi = kb / (mol_weight * atomic_unit);
    double b;
    if(selfGravity){
        b= 4*PI*G / (chi * chi);                        // constants a & b as described in Felix Antlitz BA
    }else{
        b= 0;
    }
    for(int i= 0; i<n_radius; i++){
        a.push_back(omega.at(i*n_theta)*omega.at(i*n_theta)/chi);                        // constants a & b as described in Felix Antlitz BA
    }
    int mi = get_midplane_index();
    vector<double> z;                                                                   // z coordinates
    for(int i = 0; i < n_radius; i++)
    {
      for(int j = 0; j < n_theta; j++)
      {
        z.push_back(radial_grid.at(i)*cos(theta_grid.at(j)));
      }
    }
    
    // step 1: upper half space
    
    vector<double> dz;
    for(int i = 0; i < n_radius; i++){
      for(int j = mi+1; j < n_theta; j++){
        dz.push_back(z.at(i*n_theta + j) - z.at(i*n_theta + j-1));                      // dz is negative
      }
    }
    vector<double> T;
    for(int i = 0; i < n_radius; i++){
      T.push_back(T_low.at(i));
      for(int j = 0; j < n_theta; j++){
        T.push_back(temp.at(i*n_theta + j));
      }
      T.push_back(T_up.at(i));
    }
    vector<double> mu;
    for(int i = 0; i < n_radius; i++){
      mu.push_back(1+f_d2g.at(i*n_theta));
      for(int j = 0; j < n_theta; j++){
        mu.push_back(1+f_d2g.at(i*n_theta + j));
      }
      mu.push_back(1+f_d2g.at((i+1)*n_theta-1));
    }
    vector<double> H;
    for(int i= 0; i<n_radius; i++){
        H.push_back(sqrt(chi*temp.at(i*n_theta + mi))/omega.at(i*n_theta));             // pressure scale height in midplane    
    }
    vector<double> xi0;
    for(int i= 0; i<n_radius; i++){
        xi0.push_back(log(chi*temp.at(i*n_theta + mi)*surf_dens.at(i)/(H.at(i)*sqrt(2*PI))));        
    }
    vector<double> ysol = xi0;
    for(int i= 0; i<n_radius; i++){
        ysol.push_back(0);        
    }
    vector<double> ysol_complete(n_radius*n_theta, 0);
    for(int i=0; i< n_radius; i++){
        ysol_complete.at(i*n_theta + mi) = xi0.at(i);
    }
    
    
    for(int i= mi+2; i< n_theta+1; i++){
        vector<double> T0;
        vector<double> T_l;
        vector<double> T_r;
        vector<double> mu0;
        vector<double> mu_l;
        vector<double> mu_r;
        vector<double> hz;
        vector<double> next;
        for(int j = 0; j<n_radius; j++){
            T0.push_back(T.at(j*(n_theta+2) + i));
            T_l.push_back(T.at(j*(n_theta+2) + i-1));
            T_r.push_back(T.at(j*(n_theta+2) + i+1));
            mu0.push_back(mu.at(j*(n_theta+2) + i));
            mu_l.push_back(mu.at(j*(n_theta+2) + i-1));
            mu_r.push_back(mu.at(j*(n_theta+2) + i+1));
            hz.push_back(dz.at(j*mi +(i-mi-2)));
        }
        rk4step(next, ysol, hz, 2, n_radius, T0, T_l, T_r, mu0, mu_l, mu_r, a, b);
        ysol = next;
        for(int j = 0; j<n_radius; j++){
            ysol_complete.at(j*n_theta + (i-1)) = next.at(j);
        }
    }
    
    // step 2: lower half space
    vector<double> dz2;
    for(int i = 0; i < n_radius; i++){
      for(int j = 0; j < mi; j++){
        dz2.push_back(z.at(i*n_theta + j) - z.at(i*n_theta + j+1));                      // dz is positive
      }
    }
    ysol = xi0;
    for(int i= 0; i<n_radius; i++){
        ysol.push_back(0);        
    }
    for(int i= mi; i > 0; i--){
        vector<double> T0;
        vector<double> T_l;
        vector<double> T_r;
        vector<double> mu0;
        vector<double> mu_l;
        vector<double> mu_r;
        vector<double> hz;
        vector<double> next;
        for(int j = 0; j<n_radius; j++){
            T0.push_back(T.at(j*(n_theta+2) + i));
            T_l.push_back(T.at(j*(n_theta+2) + i+1));
            T_r.push_back(T.at(j*(n_theta+2) + i-1));
            mu0.push_back(mu.at(j*(n_theta+2) + i));
            mu_l.push_back(mu.at(j*(n_theta+2) + i+1));
            mu_r.push_back(mu.at(j*(n_theta+2) + i-1));
            hz.push_back(dz2.at(j*mi +i-1));
        }
        rk4step(next, ysol, hz, 2, n_radius, T0, T_l, T_r, mu0, mu_l, mu_r, a, b);
        ysol = next;
        for(int j = 0; j<n_radius; j++){
            ysol_complete.at(j*n_theta + (i-1)) = next.at(j);
        }
    }
    
    // step 3: calculate density
    
    vector<double> p;
    vector<double> dens_log;
    for(int i=0; i< n_radius*n_theta; i++){
        p.push_back(exp(ysol_complete.at(i)));                  // pressure
        dens.at(i) = p.at(i)/(temp.at(i)*chi);                  // density
        dens_log.push_back(log(dens.at(i)));
    }
    
    vector<double> dens_log_int;
    for(int i = 0; i < n_radius; i++){
      vector<double> dens_row;
      for(int j = 0; j < n_theta; j++){
          dens_row.push_back(dens_log.at(i*n_theta + j));
      }
      vector<double> dens_row_int = interp1(dens_row, true);
      dens_log_int.insert(dens_log_int.end(), dens_row_int.begin(), dens_row_int.end());
    }
    for(int i=0; i< n_radius*(n_theta+1); i++){
        dens_int_t.at(i) = exp(dens_log_int.at(i));               // interpolated density theta
    }
    
    vector<double> dens_log_int2((n_radius+1)*n_theta,0);
    for(int i = 0; i < n_theta; i++){
      vector<double> dens_column;
      for(int j = 0; j < n_radius; j++){
          dens_column.push_back(dens_log.at(j*n_theta + i));
      }
      vector<double> dens_column_int = interp1(dens_column, true);
      for(int j = 0; j < n_radius+1; j++){
          dens_log_int2.at(j*n_theta + i) = dens_column_int.at(j);
      }
    }
    for(int i=0; i< (n_radius+1)*n_theta; i++){
        dens_int_r.at(i) = exp(dens_log_int2.at(i));               // interpolated density radius
    }
    
    return;
}

// FUNCTION THAT DEFINES THE DIFFERENTIAL EQUATION FOR VERTICAL DENSITY INTEGRATION
void get_deriv(vector<double> & res, vector<double> & y, vector<double> & hz, vector<double> & T0, vector<double> & T_l, vector<double> & T_r, vector<double> & mu0, vector<double> & mu_l, vector<double> & mu_r, vector<double> & a, double & b)
{
    for(int i=0; i<n_radius; i++){
        res.push_back(y.at(i+ n_radius));
    }
    for(int i=0; i<n_radius; i++){
        res.push_back(-a.at(i)*mu0.at(i)/T0.at(i) -b*mu0.at(i)*mu0.at(i)/(T0.at(i)*T0.at(i))*exp(y.at(i)) +((mu_r.at(i)-mu_l.at(i))/(2*hz.at(i)*mu0.at(i)) -(T_r.at(i)-T_l.at(i))/(2*hz.at(i)*T0.at(i)))*y.at(i+ n_radius));
    }
    // dT/dz and dmu/dz are central differences
    return;
}

// FUNCTION THAT DEFINES THE DIFFERENTIAL EQUATION FOR TIME DEVELOPMENT OF SURFACE DENSITY, RADIAL VELOCITY AND AZIMUTHAL VELOCITY
void get_deriv(vector<double> & res, vector<double> & y, vector<double> & hz, vector<double> & radial_grid0, vector<double> & radial_grid_l, vector<double> & radial_grid_r, vector<double> & nu0, vector<double> & nu_l, vector<double> & nu_r, vector<double> & dr_pres, vector<double> & dr_phi)
{
    
    //this cannot compile the way it is right now
    
    for(int i=0; i<n_radius; i++){
        //double rSv0 = radial_grid0.at(i) * surf_dens0.at(i) * v_radial0.at(i);
        //double rSv_l = radial_grid_l.at(i) * surf_dens_l.at(i) * v_radial_l.at(i);
        //double rSv_r = radial_grid_r.at(i) * surf_dens_r.at(i) * v_radial_r.at(i);
        
        //res.push_back(-1.0 / (2.0 * dr * radial_grid0.at(i)) * (rSv_r - rSv_l - v_radial0.at(i) / abs(v_radial0.at(i)) * (rSv_r - 2 * rSv0 + rSv_l))); // put dSigma/dt here
    }
    
    for(int i=0; i<n_radius; i++){
        //double vr0 = v_radial0.at(i);
        //double vr_l = v_radial_l.at(i);
        //double vr_r = v_radial_r.at(i);
        
        //double rSn0 = radial_grid0.at(i) * surf_dens0.at(i) * nu0.at(i);
        //double rSn_l = radial_grid_l.at(i) * surf_dens_l.at(i) * nu_l.at(i);
        //double rSn_r = radial_grid_r.at(i) * surf_dens_r.at(i) * nu_r.at(i);
        
        //double Snvr0 = surf_dens0.at(i) * nu0.at(i) * v_radial0.at(i);
        //double Snvr_l = surf_dens_l.at(i) * nu_l.at(i) * v_radial_l.at(i);
        //double Snvr_r = surf_dens_r.at(i) * nu_r.at(i) * v_radial_r.at(i);
        
        //double rSvr20 = radial_grid0.at(i) * surf_dens0.at(i) * v_radial0.at(i) * v_radial0.at(i);
        //double rSvr2_l = radial_grid_l.at(i) * surf_dens_l.at(i) * v_radial_l.at(i) * v_radial_l.at(i);
        
        //res.push_back(
          //  2.0 / 3.0 * (surf_dens0.at(i) * nu0.at(i)) * (2.0 / (dr * dr) * (vr_r - 2.0 * vr0 + vr_l) + 1.0 / (radial_grid0.at(i) * 2.0 * dr) * (vr_r - vr_l) - 2.0 * vr0 / (radial_grid0.at(i) * radial_grid0.at(i)))
          //  + 1.0 / (3.0 * radial_grid0.at(i) * dr * dr) * (rSn_r - rSn_l) * (vr_r - vr_l)
          //  - 2.0 / (3.0 * radial_grid0.at(i) * 2 * dr) * (Snvr_r - Snvr_l - vr0 / abs(vr0) * (Snvr_r - 2 * Snvr0 + Snvr_l))
          //  - 1.0 / (radial_grid0.at(i) * dr) * (rSvr20 - rSvr2_l)
          //  + surf_dens0.at(i) / radial_grid0.at(i) * v_azimuthal0.at(i) * v_azimuthal0.at(i)
          //  - dr_pres.at(i)
          //  - surf_dens0.at(i) * dr_phi.at(i)
        //); // put d(Sigma*v_r)/dt here
    }
    
    for(int i=0; i<n_radius; i++){
        //double vpr0 = v_radial0.at(i) / radial_grid0.at(i);
        //double vpr_l = v_radial_l.at(i) / radial_grid_l.at(i);
        //double vpr_r = v_radial_r.at(i) / radial_grid_r.at(i);
        
        //double Sn0 = surf_dens0.at(i) * nu0.at(i);
        //double Sn_l = surf_dens_l.at(i) * nu_l.at(i);
        //double Sn_r = surf_dens_r.at(i) * nu_r.at(i);
        
        //double rSvrvp0 = radial_grid0.at(i) * surf_dens0.at(i) * v_radial0.at(i) * v_azimuthal0.at(i);
        //double rSvrvp_l = radial_grid_l.at(i) * surf_dens_l.at(i) * v_radial_l.at(i) * v_azimuthal_l.at(i);
        //double rSvrvp_r = radial_grid_r.at(i) * surf_dens_r.at(i) * v_radial_r.at(i) * v_azimuthal_r.at(i);
        
        //res.push_back(
        //    Sn0 * (radial_grid0.at(i) / (dr * dr) * (vpr_r - 2.0 * vpr0 + vpr_l) + 3.0 / (2.0 * dr) * (vpr_r - vpr_l))
        //    + radial_grid0.at(i) / (4.0 * dr * dr) * (Sn_r - Sn_l) * (vpr_r - vpr_l)
        //    - 1.0 / (radial_grid0.at(i) * 2.0 * dr) * ((rSvrvp_r - rSvrvp_l) - v_radial0.at(i) * v_azimuthal0.at(i) / abs(v_radial0.at(i) * v_azimuthal0.at(i)) * (rSvrvp_r - 2.0 * rSvrvp0 + rSvrvp_l))
        //    - surf_dens0.at(i) / radial_grid0.at(i) * v_radial0.at(i) * v_azimuthal0.at(i)
        //); // put d(Sigma*v_phi)/dt here
    }
    
    return;
}

// FUNCTION THAT PERFORMS RUNGE-KUTTA 4TH ORDER
void rk4step(vector<double> & next, vector<double> & y, vector<double> & hz, int eq_num, int vec_length, vector<double> & param1_0, vector<double> & param1_l, vector<double> & param1_r, vector<double> & param2_0, vector<double> & param2_l, vector<double> & param2_r, vector<double> & var_param3, auto & var_param4)
{
    vector<double> k_1;
    vector<double> k_2;
    vector<double> k_3;
    vector<double> k_4;
    vector<double> y_2;
    vector<double> y_3;
    vector<double> y_4;
    
    get_deriv(k_1, y, hz, param1_0, param1_l, param1_r, param2_0, param2_l, param2_r, var_param3, var_param4);
    for(int i = 0; i< eq_num*vec_length; i++){
        y_2.push_back(y.at(i)+ 0.5*hz.at(i%vec_length)*k_1.at(i));
    }
    
    get_deriv(k_2, y_2, hz, param1_0, param1_l, param1_r, param2_0, param2_l, param2_r, var_param3, var_param4);
    for(int i = 0; i< eq_num*vec_length; i++){
        y_3.push_back(y.at(i)+ 0.5*hz.at(i%vec_length)*k_2.at(i));
    }
    
    get_deriv(k_3, y_3, hz, param1_0, param1_l, param1_r, param2_0, param2_l, param2_r, var_param3, var_param4);
    for(int i = 0; i< eq_num*vec_length; i++){
        y_4.push_back(y.at(i)+ hz.at(i%vec_length)*k_3.at(i));
    }
    
    get_deriv(k_4, y_4, hz, param1_0, param1_l, param1_r, param2_0, param2_l, param2_r, var_param3, var_param4);
    for(int i = 0; i< eq_num*vec_length; i++){
        next.push_back(y.at(i)+ hz.at(i%vec_length)/6 *(k_1.at(i)+2*k_2.at(i)+ 2*k_3.at(i)+ k_4.at(i)));
    }
    
    return;
}

// FUNCTION THAT DERIVES A GRID IN R DIRECTION
void get_dr_grid(vector<double> & dr_grid, vector<double> grid, int grid_size_r, int grid_size_theta, char method, double val1, double val2)
{
    vector<double> dr_stencil;
    int k_min=0;
    int k_max=0;
    get_dr_stencil(dr_stencil, k_min, k_max, order_of_radial_scheme);
    int o = order_of_radial_scheme/2;
    grid.reserve((grid_size_r+2*o)*grid_size_theta);
    
    switch(method){

        case 'c':
            grid.insert(grid.begin(), grid_size_theta*o, val1);
            grid.insert(grid.end(), grid_size_theta*o, val2);
        break;
        
        case 'l':
            vector<double> gridfront;
            vector<double> gridback;
            if(o == 2){
                for(int i = 0; i< grid_size_theta; i++){
                    gridfront.push_back(grid.at(i)-2*(grid.at(i+grid_size_theta)-grid.at(i)));
                }
            }
            for(int i = 0; i< grid_size_theta; i++){
                gridfront.push_back(grid.at(i)-(grid.at(i+grid_size_theta)-grid.at(i)));
                gridback.push_back(grid.at((grid_size_r-1)*grid_size_theta+i)+(grid.at((grid_size_r-1)*grid_size_theta+i)-grid.at((grid_size_r-2)*grid_size_theta+i)));
            }
            if(o == 2){
                for(int i = 0; i< grid_size_theta; i++){
                    gridback.push_back(grid.at((grid_size_r-1)*grid_size_theta+i)+2*(grid.at((grid_size_r-1)*grid_size_theta+i)-grid.at((grid_size_r-2)*grid_size_theta+i)));
                }
            }
            grid.insert(grid.begin(), gridfront.begin(), gridfront.end());
            grid.insert(grid.end(), gridback.begin(), gridback.end());
        
        break;

    }
    
    int k_dum = 0;
    for(int k = k_min; k<= k_max; k++){
        for(int i = 0; i< grid_size_r*grid_size_theta; i++){
            dr_grid.at(i) += dr_stencil.at(k_dum)* grid.at(i+ k_dum*grid_size_theta);
        }
        k_dum++;
    }
    
    return;
}

// FUNCTION THAT DERIVES A GRID IN R DIRECTION
void get_dr_grid(vector<double> & dr_grid, vector<double> grid, int grid_size_r, int grid_size_theta, char method, vector<double> val1, double val2)
{
    vector<double> dr_stencil;
    int k_min=0;
    int k_max=0;
    get_dr_stencil(dr_stencil, k_min, k_max, order_of_radial_scheme);
    int o = order_of_radial_scheme/2;
    grid.reserve((grid_size_r+2*o)*grid_size_theta);
    
    
    switch(method){

        
        case 'v':           // same as 'c' (constant) but with vector as val1
            grid.insert(grid.begin(), val1.begin(), val1.end());
            if(o==2){
                grid.insert(grid.begin(), val1.begin(), val1.end());
            }
            grid.insert(grid.end(), grid_size_theta*o, val2);
        break;

    }
    
    int k_dum = 0;
    for(int k = k_min; k<= k_max; k++){
        for(int i = 0; i< grid_size_r*grid_size_theta; i++){
            dr_grid.at(i) += dr_stencil.at(k_dum)* grid.at(i+ k_dum*grid_size_theta);
        }
        k_dum++;
    }
    
    return;
}

// FUNCTION THAT DERIVES A GRID IN POLAR DIRECTION
void get_dtheta_grid(vector<double> & dtheta_grid, vector<double> grid, int grid_size_r, int grid_size_theta, char method, vector<double> & val1, vector<double> & val2)
{
    vector<double> dtheta_stencil;
    int k_min=0;
    int k_max=0;
    get_dtheta_stencil(dtheta_stencil, k_min, k_max, order_of_theta_scheme);
    int o = order_of_theta_scheme/2;
    vector<double> grid_e(grid);
    grid_e.reserve(grid_size_r*(grid_size_theta+2*o));
    
    switch(method){

        case 'c':
            vector<double>::iterator it = grid_e.begin();
            for(int i = 0; i< grid_size_r; i++){
                grid_e.insert(it, o, val1.at(i));
                grid_e.insert(it+ grid_size_theta+o, o, val2.at(i));
                it += grid_size_theta + 2*o;
            }
        break;

    }
    
    int k_dum = 0;
    for(int k = k_min; k<= k_max; k++){
        for(int i = 0; i< grid_size_r; i++){
            for(int j = 0; j< grid_size_theta; j++){
                dtheta_grid.at(i*grid_size_theta +j) += dtheta_stencil.at(k_dum) * grid_e.at(i*(grid_size_theta+2*o)+ j+ k_dum);
            }
        }
        k_dum++;
    }
    return;
}

// FUNCTION THAT COMPUTES THE OPTICAL DEPTH
vector<double> get_tau(vector<double> & dens, vector<double> & dens_int_r, vector<double> f_d2g)
{
    vector<double> tau((n_radius+1)*n_theta,0);
    double r_trunc = mag_trunc;                                            // truncation radius (radius of solar magnetoshpere)
    
    for(int i = 0; i<n_theta; i++){
        tau.at(i) = k_gas*dens_int_r.at(i)*(r_min*au - r_trunc*r_star*r_solar)*q_tau;
    }
    for(int i = 1; i<n_radius+1; i++){
        for(int j = 0; j<n_theta; j++){
            tau.at(i*n_theta +j) = tau.at((i-1)*n_theta +j) + dens.at((i-1)*n_theta +j)*dr*(f_d2g.at((i-1)*n_theta +j)*k_dust_star+k_gas);
        }
    }
    
    return tau;
}

// FUNCTION THAT COMPUTES THE RADIATIVE FLUX
vector<double> get_F_ast(vector<double> & tau, vector<double> radial_grid)
{
    vector<double> F_ast;
    vector<double> rad_int = interp1(radial_grid, true);
    for(int i = 0; i<n_radius+1; i++){
        for(int j = 0; j<n_theta; j++){
            F_ast.push_back(pow(r_star*r_solar/rad_int.at(i),2)*sigma*pow(t_star,4) *exp(-tau.at(i*n_theta+j)));
        }
    }
    return F_ast;
}

// FUNCTION THAT COMPUTES THE DUST TO GAS RATIO
vector<double> get_f_d2g(vector<double> & dens, vector<double> & temp, vector<double> & tau)
{
    vector<double> f_d2g;
    vector<double> temp_ev;
    vector<double> tau_int_r((n_radius+1)*n_theta,0);
    for(int i = 0; i < n_theta; i++){
      vector<double> tau_column;
      for(int j = 0; j < n_radius; j++){
          tau_column.push_back(tau.at(j*n_theta + i));
      }
      vector<double> tau_column_int = interp1(tau_column, true);
      for(int j = 0; j < n_radius+1; j++){
          tau_int_r.at(j*n_theta + i) = tau_column_int.at(j);
      }
    }
    if(dustStep){
        vector<double> f_del_tau;
        for(int i=0; i<n_radius; i++){
            for(int j=0; j<n_theta; j++){
                f_del_tau.push_back(0.3/(dens.at(i*n_theta+ j)*k_dust_star*dr));
                temp_ev.push_back(2000*pow(dens.at(i*n_theta + j)/1000,0.0195));                                            // fitting model of Isella & Natta 2005 for the evaporation temperature of dust
                if(temp.at(i*n_theta + j)> temp_ev.at(i*n_theta + j)){
                    f_d2g.push_back(f_del_tau.at(i*n_theta + j)*(0.5*(1-tanh(pow(((temp.at(i*n_theta + j)-temp_ev.at(i*n_theta + j))/dustTempRange),3)))*(0.5*(1.-tanh(1-tau_int_r.at((i+1)*n_theta + j))))));
                }else{
                    f_d2g.push_back(f_d2g_0*(0.5*(1.-tanh(20.-tau_int_r.at((i+1)*n_theta + j))))+ f_del_tau.at(i*n_theta + j));
                }
            }
        }
    }else{
        if(dustTauDep){
            double f_del_tau = f_d2g_0;
            for(int i=0; i<n_radius; i++){
                for(int j=0; j<n_theta; j++){
                    temp_ev.push_back(2000*pow(dens.at(i*n_theta + j)/1000,0.0195));                                            // fitting model of Isella & Natta 2005 for the evaporation temperature of dust
                    f_d2g.push_back(f_del_tau*(0.5*(1-tanh(pow(((temp.at(i*n_theta + j)-temp_ev.at(i*n_theta + j))/dustTempRange),3)))*(0.5*(1.-tanh(1-tau_int_r.at((i+1)*n_theta + j))))));
                }
            }
        }else{
            double f_del_tau = f_d2g_0;
            for(int i=0; i<n_radius; i++){
                for(int j=0; j<n_theta; j++){
                    temp_ev.push_back(2000*pow(dens.at(i*n_theta + j)/1000,0.0195));                                            // fitting model of Isella & Natta 2005 for the evaporation temperature of dust
                    f_d2g.push_back(f_del_tau*(0.5*(1-tanh(pow(((temp.at(i*n_theta + j)-temp_ev.at(i*n_theta + j))/dustTempRange),3)))));       // tau independent f_d2g formula
                }
            }
        }
        
    }
    for(int i = 0; i< n_radius*n_theta; i++){
        if(f_d2g.at(i)< f_d2g_min){
            f_d2g.at(i) = f_d2g_min;
        }
        if(f_d2g.at(i)> f_d2g_0){
            f_d2g.at(i) = f_d2g_0;
        }
    }
    return f_d2g;
}

// FUNCTION THAT CALCULATES THE DIFFUSION CONSTANTS FOR THE TEMPERATURE DIFFUSION
void get_D_T(vector<double> & D_T_r, vector<double> &  D_T_t, vector<double> &  dens_int_r, vector<double> & dens_int_t, vector<double> &  nu, bool heat_cond)
{
    if(heat_cond){
        vector<double> nu_int_r((n_radius+1)*n_theta,0);
        for(int i = 0; i < n_theta; i++){
            vector<double> nu_column;
            for(int j = 0; j < n_radius; j++){
                nu_column.push_back(nu.at(j*n_theta + i));
            }
            vector<double> nu_column_int = interp1(nu_column, true);
            for(int j = 0; j < n_radius+1; j++){
                nu_int_r.at(j*n_theta + i) = nu_column_int.at(j);
            }
        }
        vector<double> nu_int_t;
        for(int i = 0; i < n_radius; i++){
            vector<double> nu_row;
            for(int j = 0; j < n_theta; j++){
                nu_row.push_back(nu.at(i*n_theta + j));
            }
            vector<double> nu_row_int = interp1(nu_row, true);
            nu_int_t.insert(nu_int_t.end(), nu_row_int.begin(), nu_row_int.end());
        }
        for(int i = 0; i< (n_radius+1)*n_theta; i++){
            D_T_r.at(i) =  nu_int_r.at(i) * cp/Pr;
        }
        for(int i = 0; i< n_radius*(n_theta+1); i++){
            D_T_t.at(i) =  nu_int_t.at(i) * cp/Pr;
        }
    }
    
    return;
}

// FUNCTION THAT CALCULATES THE DIFFUSION CONSTANTS FOR THE RADIATION ENERGY DIFFUSION
void get_D_Er(vector<double> & D_Er_r, vector<double> & D_Er_t, vector<double> & dens_int_r, vector<double> & dens_int_t, vector<double> & Er, vector<double> & f_d2g, vector<double> & Er_up, vector<double> & Er_low, vector<double> & Er_0f, vector<double> & radial_grid){
    
        // interpolate Er and f_d2g radially and polarly
        vector<double> Er_int_r((n_radius+1)*n_theta,0);
        vector<double> f_d2g_int_r((n_radius+1)*n_theta,0);
        for(int i = 0; i < n_theta; i++){
            vector<double> Er_column;
            vector<double> f_d2g_column;
            for(int j = 0; j < n_radius; j++){
                Er_column.push_back(Er.at(j*n_theta + i));
                f_d2g_column.push_back(f_d2g.at(j*n_theta + i));
            }
            vector<double> Er_column_int = interp1(Er_column, true);
            vector<double> f_d2g_column_int = interp1(f_d2g_column, true);
            for(int j = 0; j < n_radius+1; j++){
                Er_int_r.at(j*n_theta + i) = Er_column_int.at(j);
                f_d2g_int_r.at(j*n_theta + i) = f_d2g_column_int.at(j);
            }
        }
        vector<double> Er_int_t;
        vector<double> f_d2g_int_t;
        for(int i = 0; i < n_radius; i++){
            vector<double> Er_row;
            vector<double> f_d2g_row;
            for(int j = 0; j < n_theta; j++){
                Er_row.push_back(Er.at(i*n_theta + j));
                f_d2g_row.push_back(f_d2g.at(i*n_theta + j));
            }
            vector<double> Er_row_int = interp1(Er_row, true);
            vector<double> f_d2g_row_int = interp1(f_d2g_row, true);
            Er_int_t.insert(Er_int_t.end(), Er_row_int.begin(), Er_row_int.end());
            f_d2g_int_t.insert(f_d2g_int_t.end(), f_d2g_row_int.begin(), f_d2g_row_int.end());
        }
        
        vector<double> Er_up_int = interp1(Er_up, true);                                // interpolates Er_up linear
        vector<double> Er_low_int = interp1(Er_low, true);                              // interpolates Er_low linear
        vector<double> Er_0f_int = interp1(Er_0f, true);                              // interpolates Er_0f linear
        
        // ensure f_d2g is not extrapolated under the minimum
        for(int i = 0; i< n_radius*(n_theta+1); i++){
            if(f_d2g_int_t.at(i)< f_d2g_min){
                f_d2g_int_t.at(i) = f_d2g_min;
            }
        }
        
        vector<double> dr_Er_int_t(n_radius*(n_theta+1),0);
        vector<double> dtheta_Er_int_t(n_radius*(n_theta+1),0);
        vector<double> grad_Er_t(n_radius*(n_theta+1),0);
        vector<double> sig_R_t(n_radius*(n_theta+1),0);
        get_dr_grid(dr_Er_int_t, Er_int_t, n_radius, n_theta+1, 'v', Er_0f_int, Er_0b);
        get_dtheta_grid(dtheta_Er_int_t, Er_int_t, n_radius, n_theta+1, 'c', Er_low, Er_up);
        for(int i=0; i< n_radius; i++){
            for(int j=0; j< n_theta+1; j++){
                grad_Er_t.at(i*(n_theta+1)+j) = sqrt(pow(dr_Er_int_t.at(i*(n_theta+1)+j),2)+pow(dtheta_Er_int_t.at(i*(n_theta+1)+j)/radial_grid.at(i),2));
                sig_R_t.at(i*(n_theta+1)+j) = dens_int_t.at(i*(n_theta+1)+j) * f_d2g_int_t.at(i*(n_theta+1)+j) * k_dust_rim + dens_int_t.at(i*(n_theta+1)+j) * k_gas;
            }
        }
        
        vector<double> radial_grid_int = interp1(radial_grid, true);
        vector<double> dr_Er_int_r((n_radius+1)*n_theta,0);
        vector<double> dtheta_Er_int_r((n_radius+1)*n_theta,0);
        vector<double> grad_Er_r((n_radius+1)*n_theta,0);
        vector<double> sig_R_r((n_radius+1)*n_theta,0);
        get_dr_grid(dr_Er_int_r, Er_int_r, n_radius+1, n_theta, 'v', Er_0f, Er_0b);
        get_dtheta_grid(dtheta_Er_int_r, Er_int_r, n_radius+1, n_theta, 'c', Er_low_int, Er_up_int);
        for(int i=0; i< n_radius+1; i++){
            for(int j=0; j< n_theta; j++){
                grad_Er_r.at(i*n_theta+j) = sqrt(pow(dr_Er_int_r.at(i*n_theta+j),2)+pow(dtheta_Er_int_r.at(i*n_theta+j)/radial_grid_int.at(i),2));
                sig_R_r.at(i*n_theta+j) = dens_int_r.at(i*n_theta+j) * f_d2g_int_r.at(i*n_theta+j) * k_dust_rim + dens_int_r.at(i*n_theta+j) * k_gas;
            }
        }
        
        vector<double> lambda_r = get_lambda(grad_Er_r, Er_int_r, sig_R_r);
        vector<double> lambda_t = get_lambda(grad_Er_t, Er_int_t, sig_R_t);
        
        for(int i=0; i< n_radius; i++){
            for(int j=0; j< n_theta+1; j++){
                D_Er_t.at(i*(n_theta+1)+j) = c0* lambda_t.at(i*(n_theta+1)+j) / sig_R_t.at(i*(n_theta+1)+j);
            }
        }
        for(int i=0; i< n_radius+1; i++){
            for(int j=0; j< n_theta; j++){
                D_Er_r.at(i*n_theta+j) = c0* lambda_r.at(i*n_theta+j) / sig_R_r.at(i*n_theta+j);
            }
        }
    
    return;
}

// FUNCTION THAT CONPUTES THE FLUX LIMITER
vector<double> get_lambda(vector<double> & grad_Er, vector<double> & Er, vector<double> & sig_R){
    
    vector<double> R;
    vector<double> lambda;
    int gridsize = grad_Er.size();
    for(int i=0; i< gridsize; i++){
        R.push_back(grad_Er.at(i)/(sig_R.at(i)*Er.at(i)));
        lambda.push_back((2+R.at(i))/(6+3*R.at(i)+R.at(i)*R.at(i)));
    }

    return lambda;
}


// FUNCTION THAT RETURNS THE STENCIL FOR RADIAL DERIVATION
void get_dr_stencil(vector<double> & dr_stencil, int & k_min, int & k_max, int order){


    switch(order){

        case 2:
            
        dr_stencil = {(-1./2.)/dr, 0, (1./2.)/dr};
        k_min = -1;
        k_max = 1;
        break;
        
        case 4:
            
        dr_stencil = {(1./12.)/dr, (-2./3.)/dr, 0, (2./3.)/dr, (-1./12.)/dr};              // five point stencil
        k_min = -2;
        k_max = 2;
        break;

    }

    return;
}

// FUNCTION THAT RETURNS THE STENCIL FOR POLAR DERIVATION
void get_dtheta_stencil(vector<double> & dtheta_stencil, int & k_min, int & k_max, int order){


    switch(order){

        case 2:
            
        dtheta_stencil = {(-1./2.)/dtheta, 0, (1./2.)/dtheta};
        k_min = -1;
        k_max = 1;
        break;
        
        case 4:
            
        dtheta_stencil = {(1./12.)/dtheta, (-2./3.)/dtheta, 0, (2./3.)/dtheta, (-1./12.)/dtheta};              // five point stencil
        k_min = -2;
        k_max = 2;
        break;

    }

    return;
}

// FUNCTION THAT BUILDS THE RHS VECTOR B
void build_rhs(vector<double> & b, vector<double> & temp, vector<double> & dens, vector<double> & Er, vector<double> & dr_omega, vector<double> & nu, vector<double> & f_d2g, vector<double> & F_ast, vector<double> & D_Er_r, vector<double> & D_Er_t, double dt, bool visc_heat, vector<double> & Er_up, vector<double> & Er_low, vector<double> & Er_0f, vector<double> & radial_grid, vector<double> & polar_grid){
    
    //int size = 2*n_radius*n_theta;
    vector<double> rig;            // radial inwards coefficients
    vector<double> rog;            // radial outwards coefficients
    vector<double> tdg;            // polar downwards coefficients
    vector<double> tug;            // polar upwards coefficients
    vector<double> radial_grid_int = interp1(radial_grid, true);
    vector<double> polar_grid_int = interp1(polar_grid, true);
    
    for(int i = 0; i< n_radius; i++){
        for(int j = 0; j < n_theta; j++){
            // geometry coefficients
            rig.push_back(pow(radial_grid_int.at(i),2)/((1./3.)*(pow(radial_grid_int.at(i+1),3) - pow(radial_grid_int.at(i),3))*dr));
            rog.push_back(pow(radial_grid_int.at(i+1),2)/((1./3.)*(pow(radial_grid_int.at(i+1),3) - pow(radial_grid_int.at(i),3))*dr));
            tdg.push_back(1./pow(radial_grid.at(i),2) * (abs(sin(polar_grid_int.at(j)))/(abs(cos(polar_grid_int.at(j))-cos(polar_grid_int.at(j+1)))*dtheta)));
            tug.push_back(1./pow(radial_grid.at(i),2) * (abs(sin(polar_grid_int.at(j+1)))/(abs(cos(polar_grid_int.at(j))-cos(polar_grid_int.at(j+1)))*dtheta)));
            
            // rhs vector, temperature
            b.at((i*n_theta +j)*2) = (cV*temp.at(i*n_theta +j))/dt 
                                    +((f_d2g.at(i*n_theta +j) * k_dust_rim + k_gas))*c0*ar*3 * pow(temp.at(i*n_theta +j),4) 
                                    + ((F_ast.at((i+1)*n_theta +j) + F_ast.at(i*n_theta +j))/2.)* (f_d2g.at(i*n_theta +j) * k_dust_star + k_gas)
                                    + visc_heat* nu.at(i*n_theta +j)* pow(radial_grid.at(i)*dr_omega.at(i*n_theta +j),2);
                                    
            // rhs vector, radiation energy
            b.at((i*n_theta +j)*2+1)= Er.at(i*n_theta +j)/dt 
                                      -(dens.at(i*n_theta +j)* (f_d2g.at(i*n_theta +j)* k_dust_rim +k_gas))*c0*ar*3 *pow(temp.at(i*n_theta +j),4);
        }
    }
    
    // fixed boundary conditions
    for(int j = 0; j< n_theta; j++){
        b.at(j*2+1) += D_Er_r.at(j)*rig.at(j)*Er_0f.at(j);                                                                  // inner radius
        b.at(((n_radius-1)*n_theta +j)*2+1) += D_Er_r.at(n_radius*n_theta + j)* rog.at((n_radius-1)*n_theta +j)* Er_0b;     // outer radius
    }
    for(int i = 0; i< n_radius; i++){
        b.at((i*n_theta)*2+1) += D_Er_t.at(i*(n_theta+1)) * tdg.at(i*n_theta) *Er_low.at(i);                                    // lower theta
        b.at(((i+1)*n_theta-1)*2+1) += D_Er_t.at((i+1)*(n_theta+1)-1)*tug.at((i+1)*n_theta-1) *Er_up.at(i);                     // upper theta
    }
    
    return;
}

// FUNCTION THAT BUILDS THE MATRIX
void build_matrix(vector<double> & Ax, vector<double> & D_T_r, vector<double> & D_T_t, vector<double> & D_Er_r, vector<double> & D_Er_t, vector<double> & temp, vector<double> & dens, vector<double> & f_d2g, double dt, vector<double> & radial_grid, vector<double> & polar_grid){
    
    vector<double> rig;            // radial inwards coefficients
    vector<double> rog;            // radial outwards coefficients
    vector<double> tdg;            // polar downwards coefficients
    vector<double> tug;            // polar upwards coefficients
    vector<double> radial_grid_int = interp1(radial_grid, true);
    vector<double> polar_grid_int = interp1(polar_grid, true);
    
    //int size = 2*n_radius*n_theta;
    
    for(int i = 0; i< n_radius; i++){
        for(int j = 0; j < n_theta; j++){
            // geometry coefficients
            rig.push_back(pow(radial_grid_int.at(i),2)/((1./3.)*(pow(radial_grid_int.at(i+1),3) - pow(radial_grid_int.at(i),3))*dr));
            rog.push_back(pow(radial_grid_int.at(i+1),2)/((1./3.)*(pow(radial_grid_int.at(i+1),3) - pow(radial_grid_int.at(i),3))*dr));
            tdg.push_back(1./pow(radial_grid.at(i),2) * (abs(sin(polar_grid_int.at(j)))/(abs(cos(polar_grid_int.at(j))-cos(polar_grid_int.at(j+1)))*dtheta)));
            tug.push_back(1./pow(radial_grid.at(i),2) * (abs(sin(polar_grid_int.at(j+1)))/(abs(cos(polar_grid_int.at(j))-cos(polar_grid_int.at(j+1)))*dtheta)));
            
        }
    }
    
    // matrix vector Ax
    Ax.at(0) = (cV)/dt +((f_d2g.at(0)*k_dust_rim +k_gas))*c0*ar*4.*pow(temp.at(0),3.) + D_T_r.at(0+n_theta) *rog.at(0) +D_T_r.at(0)*rig.at(0) + D_T_t.at(0+1)*tug.at(0) +D_T_t.at(0)*tdg.at(0);                                     // temperature equation T(i,j)
    Ax.at(1) = -(dens.at(0)*(f_d2g.at(0)*k_dust_rim +k_gas))*c0*ar*4.*pow(temp.at(0),3.);                                                                                                                                           // radiation energy equation T(i,j)
    Ax.at(2) = -D_T_t.at(1)*tdg.at(1);                                                                                                                                                                                              // temperature equation T(i,j-1)
    Ax.at(3) = -D_T_r.at(n_theta)*rig.at(n_theta);                                                                                                                                                                                  // temperature equation T(i-1,j)
    // now second part
    Ax.at(4) = -((f_d2g.at(0)*k_dust_rim +k_gas))*c0;                                                                                                                                                                               // temperature equation Er(i,j)            
    Ax.at(5) = +1/dt +(dens.at(0)*(f_d2g.at(0)*k_dust_rim +k_gas))*c0 + D_Er_r.at(0+n_theta)*rog.at(0) + D_Er_r.at(0)*rig.at(0) + D_Er_t.at(0+1)*tug.at(0) + D_Er_t.at(0)*tdg.at(0);                                                // radiation energy equation Er(i,j)
    Ax.at(6) = -D_Er_t.at(1)*tdg.at(1);                                                                                                                                                                                             // radiation energy equation Er(i,j-1)
    Ax.at(7) = -D_Er_r.at(n_theta)*rig.at(n_theta);                                                                                                                                                                                 // radiation energy equation Er(i-1,j)
    int j = 8;
    for(int i = 1; i < n_theta; i++){
        Ax.at(j) = -D_T_t.at(i)*tug.at(i-1);                                                                                                                                                                                        // temperature equation T(i,j+1)
        j++;
        Ax.at(j) = (cV)/dt +((f_d2g.at(i)*k_dust_rim +k_gas))*c0*ar*4.*pow(temp.at(i),3.) + D_T_r.at(i+n_theta) *rog.at(i) +D_T_r.at(i)*rig.at(i) + D_T_t.at(i+1)*tug.at(i) +D_T_t.at(i)*tdg.at(i);                                 // temperature equation T(i,j)
        j++;
        Ax.at(j) = -(dens.at(i)*(f_d2g.at(i)*k_dust_rim +k_gas))*c0*ar*4.*pow(temp.at(i),3.);                                                                                                                                       // radiation energy equation T(i,j)
        j++;
        Ax.at(j) = -D_T_t.at(i+1)*tdg.at(i+1);                                                                                                                                                                                      // temperature equation T(i,j-1)
        j++;
        Ax.at(j) = -D_T_r.at(i+n_theta)*rig.at(i+n_theta);                                                                                                                                                                          // temperature equation T(i-1,j)
        j++;
        // now second part
        Ax.at(j) = -D_Er_t.at(i)*tug.at(i-1);                                                                                                                                                                                       // radiation energy equation Er(i,j+1)
        j++;
        Ax.at(j) = -((f_d2g.at(i)*k_dust_rim +k_gas))*c0;                                                                                                                                                                           // temperature equation Er(i,j)        
        j++;
        Ax.at(j) = +1/dt +(dens.at(i)*(f_d2g.at(i)*k_dust_rim +k_gas))*c0 + D_Er_r.at(i+n_theta)*rog.at(i) + D_Er_r.at(i)*rig.at(i) + D_Er_t.at(i+1)*tug.at(i) + D_Er_t.at(i)*tdg.at(i);                                            // radiation energy equation Er(i,j)
        j++;
        Ax.at(j) = -D_Er_t.at(i+1)*tdg.at(i+1);                                                                                                                                                                                     // radiation energy equation Er(i,j-1)
        j++;
        Ax.at(j) = -D_Er_r.at(i+n_theta)*rig.at(i+n_theta);                                                                                                                                                                         // radiation energy equation Er(i-1,j)
        j++;
    }
    for(int i = 1; i < n_radius-1; i++){
        for(int k = 0; k < n_theta; k++){
            Ax.at(j) = -D_T_r.at(i*n_theta +k)*rog.at(i*n_theta +k - n_theta);                                                                                                                                                      // temperature equation T(i+1,j)
            j++;
            Ax.at(j) = -D_T_t.at(i*(n_theta+1) +k)*tug.at(i*n_theta +k -1);                                                                                                                                                         // temperature equation T(i,j+1)
            j++;
            Ax.at(j) = +(cV)/dt +((f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0*ar*4.*pow(temp.at(i*n_theta +k),3.) 
                       + D_T_r.at(i*n_theta +k +n_theta) *rog.at(i*n_theta +k) +D_T_r.at(i*n_theta +k)*rig.at(i*n_theta +k) + D_T_t.at(i*(n_theta+1) +k+1)*tug.at(i*n_theta +k) +D_T_t.at(i*(n_theta+1) +k)*tdg.at(i*n_theta +k);   // temperature equation T(i,j)
            j++;
            Ax.at(j) = -(dens.at(i*n_theta +k)*(f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0*ar*4.*pow(temp.at(i*n_theta +k),3.);                                                                                                  // radiation energy equation T(i,j)
            j++;
            Ax.at(j) = -D_T_t.at(i*(n_theta+1) +k +1)*tdg.at(i*n_theta +k+1);                                                                                                                                                       // temperature equation T(i,j-1);
            j++;
            Ax.at(j) = -D_T_r.at(i*n_theta +k +n_theta)*rig.at(i*n_theta +k +n_theta);                                                                                                                                              // temperature equation T(i-1,j)
            j++;
            // now second part
            Ax.at(j) = -D_Er_r.at(i*n_theta +k)*rog.at(i*n_theta +k - n_theta);                                                                                                                                                     // radiation energy equation Er(i+1,j)
            j++;
            Ax.at(j) = -D_Er_t.at(i*(n_theta+1) +k)*tug.at(i*n_theta +k -1);                                                                                                                                                        // radiation energy equation Er(i,j+1)
            j++;
            Ax.at(j) = -((f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0;                                                                                                                                                            // temperature equation Er(i,j)        
            j++;
            Ax.at(j) = +1/dt +(dens.at(i*n_theta +k)*(f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0 
                       + D_Er_r.at(i*n_theta +k +n_theta)*rog.at(i*n_theta +k) + D_Er_r.at(i*n_theta +k)*rig.at(i*n_theta +k) + D_Er_t.at(i*(n_theta+1)+k+1)*tug.at(i*n_theta +k) + D_Er_t.at(i*(n_theta+1)+k)*tdg.at(i*n_theta +k);// radiation energy equation Er(i,j)
            j++;
            Ax.at(j) = -D_Er_t.at(i*(n_theta+1) +k +1)*tdg.at(i*n_theta +k+1);                                                                                                                                                      // radiation energy equation Er(i,j-1)
            j++;
            Ax.at(j) = -D_Er_r.at(i*n_theta +k +n_theta)*rig.at(i*n_theta +k +n_theta);                                                                                                                                             // radiation energy equation Er(i-1,j)
            j++;
        }
    }
    for(int k = 0; k < n_theta-1; k++){
        int i = n_radius-1;
        Ax.at(j) = -D_T_r.at(i*n_theta +k)*rog.at(i*n_theta +k - n_theta);                                                                                                                                                          // temperature equation T(i+1,j)
        j++;
        Ax.at(j) = -D_T_t.at(i*(n_theta+1) +k)*tug.at(i*n_theta +k -1);                                                                                                                                                             // temperature equation T(i,j+1)
        j++;
        Ax.at(j) = +(cV)/dt +((f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0*ar*4.*pow(temp.at(i*n_theta +k),3.) 
                   + D_T_r.at(i*n_theta +k +n_theta) *rog.at(i*n_theta +k) +D_T_r.at(i*n_theta +k)*rig.at(i*n_theta +k) + D_T_t.at(i*(n_theta+1) +k+1)*tug.at(i*n_theta +k) +D_T_t.at(i*(n_theta+1) +k)*tdg.at(i*n_theta +k);       // temperature equation T(i,j)
        j++;
        Ax.at(j) = -(dens.at(i*n_theta +k)*(f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0*ar*4.*pow(temp.at(i*n_theta +k),3.);                                                                                                      // radiation energy equation T(i,j)
        j++;
        Ax.at(j) = -D_T_t.at(i*(n_theta+1) +k +1)*tdg.at(i*n_theta +k+1);                                                                                                                                                           // temperature equation T(i,j-1);
        j++;
        // now second part
        Ax.at(j) = -D_Er_r.at(i*n_theta +k)*rog.at(i*n_theta +k - n_theta);                                                                                                                                                         // radiation energy equation Er(i+1,j)
        j++;
        Ax.at(j) = -D_Er_t.at(i*(n_theta+1) +k)*tug.at(i*n_theta +k -1);                                                                                                                                                            // radiation energy equation Er(i,j+1)
        j++;
        Ax.at(j) = -((f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0;                                                                                                                                                                // temperature equation Er(i,j)        
        j++;
        Ax.at(j) = +1/dt +(dens.at(i*n_theta +k)*(f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0 
                   + D_Er_r.at(i*n_theta +k +n_theta)*rog.at(i*n_theta +k) + D_Er_r.at(i*n_theta +k)*rig.at(i*n_theta +k) + D_Er_t.at(i*(n_theta+1)+k+1)*tug.at(i*n_theta +k) + D_Er_t.at(i*(n_theta+1)+k)*tdg.at(i*n_theta +k);    // radiation energy equation Er(i,j)
        j++;
        Ax.at(j) = -D_Er_t.at(i*(n_theta+1) +k +1)*tdg.at(i*n_theta +k+1);                                                                                                                                                          // radiation energy equation Er(i,j-1)
        j++;
    }
    int i = n_radius-1;
    int k = n_theta-1;
    Ax.at(j) = -D_T_r.at(i*n_theta +k)*rog.at(i*n_theta +k - n_theta);                                                                                                                                                              // temperature equation T(i+1,j)
    j++;
    Ax.at(j) = -D_T_t.at(i*(n_theta+1) +k)*tug.at(i*n_theta +k -1);                                                                                                                                                                 // temperature equation T(i,j+1)
    j++;
    Ax.at(j) = +(cV)/dt +((f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0*ar*4.*pow(temp.at(i*n_theta +k),3.) 
               + D_T_r.at(i*n_theta +k +n_theta) *rog.at(i*n_theta +k) +D_T_r.at(i*n_theta +k)*rig.at(i*n_theta +k) + D_T_t.at(i*(n_theta+1) +k+1)*tug.at(i*n_theta +k) +D_T_t.at(i*(n_theta+1) +k)*tdg.at(i*n_theta +k);           // temperature equation T(i,j)
    j++;
    Ax.at(j) = -(dens.at(i*n_theta +k)*(f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0*ar*4.*pow(temp.at(i*n_theta +k),3.);                                                                                                          // radiation energy equation T(i,j)
    j++;
    // now second part
    Ax.at(j) = -D_Er_r.at(i*n_theta +k)*rog.at(i*n_theta +k - n_theta);                                                                                                                                                             // radiation energy equation Er(i+1,j)
    j++;
    Ax.at(j) = -D_Er_t.at(i*(n_theta+1) +k)*tug.at(i*n_theta +k -1);                                                                                                                                                                // radiation energy equation Er(i,j+1)
    j++;
    Ax.at(j) = -((f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0;                                                                                                                                                                    // temperature equation Er(i,j)        
    j++;
    Ax.at(j) = +1/dt +(dens.at(i*n_theta +k)*(f_d2g.at(i*n_theta +k)*k_dust_rim +k_gas))*c0 
               + D_Er_r.at(i*n_theta +k +n_theta)*rog.at(i*n_theta +k) + D_Er_r.at(i*n_theta +k)*rig.at(i*n_theta +k) + D_Er_t.at(i*(n_theta+1)+k+1)*tug.at(i*n_theta +k) + D_Er_t.at(i*(n_theta+1)+k)*tdg.at(i*n_theta +k);        // radiation energy equation Er(i,j)
    
    // remove periodic BC
    int p = 8+ 5*2*(n_theta-1);             // number of matrix entries before upper diagonal sets in
    Ax.at(p-7) = 0;
    Ax.at(p-2) = 0;
    for(int i = 0; i < n_radius-2; i++){
        Ax.at(p+1+i*(2*6*n_theta)) = 0;
        Ax.at(p+7+i*(2*6*n_theta)) = 0;
        Ax.at(p-2+(i+1)*(2*6*n_theta)) = 0;
        Ax.at(p-8+(i+1)*(2*6*n_theta)) = 0;
    }
    int q = 8+ 5*2*(n_theta-1)+ 6*2*(n_radius-2)*n_theta;             // number of matrix entries before lower diagonal stops
    Ax.at(q+1) = 0;
    Ax.at(q+6) = 0;
    
    // zero gradient for temp
        // inner radius
    Ax.at(0) += -D_T_r.at(0)*rig.at(0);
    for(int i=1; i<n_theta; i++){
        Ax.at(i*10 -1) += -D_T_r.at(i)*rig.at(i);
    }
        // outer radius
    i = n_radius-1;
    for(int k=0; k<n_theta; k++){
        Ax.at(q+2 +k*10) += -D_T_r.at(i*n_theta +k +n_theta) *rog.at(i*n_theta +k);
    }
        // lower theta
    Ax.at(0) += -D_T_t.at(0)*tdg.at(0); 
    k = 0;
    for(int i=1; i<n_radius; i++){
        Ax.at(p+2 + (i-1)*n_theta*2*6) += -D_T_t.at(i*(n_theta+1) +k)*tdg.at(i*n_theta +k);
    }
        // upper theta
    Ax.at(p- 9) += -D_T_t.at(n_theta)*tug.at(n_theta-1);
    k= n_theta-1;
    for(int i=1; i<n_radius -1; i++){
        Ax.at(p-10 + i*n_theta*2*6) += -D_T_t.at(i*(n_theta+1) +k+1)*tug.at(i*n_theta +k);
    }
    i= n_radius-1;
    Ax.at(q+2 +k*10) += -D_T_t.at(i*(n_theta+1) +k+1)*tug.at(i*n_theta +k);
    
    return;
}

// FUNCTION THAT PREBUILDS THE MATRIX
void pre_build_matrix(vector<int> & Ap, vector<int> & Ai){
    
    // matrix vector Ap
    int size = 2*n_radius*n_theta;
    
    Ap.at(0) = 0;
    Ap.at(1) = Ap.at(0) + 4;
    Ap.at(2) = Ap.at(1) + 4;
    for(int i = 3; i <= 2*n_theta; i++){
        Ap.at(i) = Ap.at(i-1) + 5;
    }
    for(int i = 2*n_theta+1; i <= size - 2*n_theta; i++){
        Ap.at(i) = Ap.at(i-1) + 6;
    }
    for(int i = size - 2*n_theta +1; i < size; i++){
        Ap.at(i) = Ap.at(i-1) + 5;
    }
    Ap.at(size- 1) = Ap.at(size-2) + 4;
    Ap.at(size) = Ap.at(size-1) + 4;
    
    //vector<double> Ap2(Ap.begin(), Ap.end());
    
    //stringstream Apfilename;
    //Apfilename << "Ap.dat";
    //write2d(Apfilename.str(), Ap2, size +1, 1);
    
    // matrix vector Ai
    Ai.at(0) = 0;
    Ai.at(1) = 1;
    Ai.at(2) = 2;
    Ai.at(3) = 2*n_theta;
    Ai.at(4) = 0;             // now second part
    Ai.at(5) = 1;
    Ai.at(6) = 3;
    Ai.at(7) = 1+2*n_theta;
    int j = 8;
    for(int i = 2; i < 2*n_theta; i++){
        Ai.at(j) = i-2;
        j++;
        Ai.at(j) = i;
        j++;
        Ai.at(j) = i+1;
        j++;
        Ai.at(j) = i+2;
        j++;
        Ai.at(j) = i+2*n_theta;
        j++;
        i++;              // now second part
        Ai.at(j) = i-2;
        j++;
        Ai.at(j) = i-1;
        j++;
        Ai.at(j) = i;
        j++;
        Ai.at(j) = i+2;
        j++;
        Ai.at(j) = i+2*n_theta;
        j++;
    }
    for(int i = 2*n_theta; i < size-2*n_theta; i++){
        Ai.at(j) = i-2*n_theta;
        j++;
        Ai.at(j) = i-2;
        j++;
        Ai.at(j) = i;
        j++;
        Ai.at(j) = i+1;
        j++;
        Ai.at(j) = i+2;
        j++;
        Ai.at(j) = i+2*n_theta;
        j++;
        i++;              // now second part
        Ai.at(j) = i-2*n_theta;
        j++;
        Ai.at(j) = i-2;
        j++;
        Ai.at(j) = i-1;
        j++;
        Ai.at(j) = i;
        j++;
        Ai.at(j) = i+2;
        j++;
        Ai.at(j) = i+2*n_theta;
        j++;
    }
    for(int i = size-2*n_theta; i < size-2; i++){
        Ai.at(j) = i-2*n_theta;
        j++;
        Ai.at(j) = i-2;
        j++;
        Ai.at(j) = i;
        j++;
        Ai.at(j) = i+1;
        j++;
        Ai.at(j) = i+2;
        j++;
        i++;              // now second part
        Ai.at(j) = i-2*n_theta;
        j++;
        Ai.at(j) = i-2;
        j++;
        Ai.at(j) = i-1;
        j++;
        Ai.at(j) = i;
        j++;
        Ai.at(j) = i+2;
        j++;
    }
    int i = size-2;
    Ai.at(j) = i-2*n_theta;
    j++;
    Ai.at(j) = i-2;
    j++;
    Ai.at(j) = i;
    j++;
    Ai.at(j) = i+1;
    j++;
    i++;              // now second part
    Ai.at(j) = i-2*n_theta;
    j++;
    Ai.at(j) = i-2;
    j++;
    Ai.at(j) = i-1;
    j++;
    Ai.at(j) = i;
    
    return;
}

// FUNCTION THAT REORGANIZES x INTO temp AND Er
void get_new_fields(vector<double> & x, vector<double> & temp, vector<double> & Er)
{
    
    for(int i = 0; i<n_radius*n_theta; i++){
        temp.at(i) = x.at(2*i);
        Er.at(i) = x.at(2*i+1);
    }

    return;
}

// FUNCTION THAT PREBUILDS THE MATRIX FOR DUST DIFFUSION
void pre_build_dust_matrix(vector<int> & Ap2, vector<int> & Ai2){
    
    // matrix vector Ap2
    int size = n_radius*n_theta;
    
    Ap2.at(0) = 0;
    Ap2.at(1) = Ap2.at(0) + 3;
    for(int i = 2; i <= n_theta; i++){
        Ap2.at(i) = Ap2.at(i-1) + 4;
    }
    for(int i = n_theta+1; i <= size - n_theta; i++){
        Ap2.at(i) = Ap2.at(i-1) + 5;
    }
    for(int i = size - n_theta +1; i < size; i++){
        Ap2.at(i) = Ap2.at(i-1) + 4;
    }
    Ap2.at(size) = Ap2.at(size-1) + 3;
    
    //vector<double> Ap2(Ap.begin(), Ap.end());
    
    //stringstream Apfilename;
    //Apfilename << "Ap.dat";
    //write2d(Apfilename.str(), Ap2, size +1, 1);
    
    // matrix vector Ai2
    Ai2.at(0) = 0;
    Ai2.at(1) = 1;
    Ai2.at(2) = n_theta;
    int j = 3;
    for(int i = 1; i < n_theta; i++){
        Ai2.at(j) = i-1;
        j++;
        Ai2.at(j) = i;
        j++;
        Ai2.at(j) = i+1;
        j++;
        Ai2.at(j) = i+n_theta;
        j++;
    }
    for(int i = n_theta; i < size-n_theta; i++){
        Ai2.at(j) = i-n_theta;
        j++;
        Ai2.at(j) = i-1;
        j++;
        Ai2.at(j) = i;
        j++;
        Ai2.at(j) = i+1;
        j++;
        Ai2.at(j) = i+n_theta;
        j++;
    }
    for(int i = size-n_theta; i < size-1; i++){
        Ai2.at(j) = i-n_theta;
        j++;
        Ai2.at(j) = i-1;
        j++;
        Ai2.at(j) = i;
        j++;
        Ai2.at(j) = i+1;
        j++;
    }
    int i = size-1;
    Ai2.at(j) = i-n_theta;
    j++;
    Ai2.at(j) = i-1;
    j++;
    Ai2.at(j) = i;
    
    return;
}

void get_diffusion(vector<double> & dens, vector<double> & f_d2g, vector<double> & nu, vector<double> & radial_grid, vector<double> & polar_grid, vector<int> & Ap2, vector<int> & Ai2, vector<double> & Ax2, int matrix_size2, double Info[], double Control[]){
    
    vector<double> x2(n_radius * n_theta, 0);
    int status;
    void *Numeric;
    void *Symbolic;
    
    
    //------------calculate RHS 
    
    int size = n_radius*n_theta;
    //gas density
    vector<double> b = dens;
    //dust density
    vector<double> c;
    for(int i = 0; i < size; i++){
        c.push_back(dens.at(i)*f_d2g.at(i));
    }
    
    //------------diffusion coefficients
    
    // interpolate nu radially and polarly
        vector<double> nu_r((n_radius+1)*n_theta,0);
        for(int i = 0; i < n_theta; i++){
            vector<double> nu_column;
            for(int j = 0; j < n_radius; j++){
                nu_column.push_back(nu.at(j*n_theta + i));
            }
            vector<double> nu_column_int = interp1(nu_column, true);
            for(int j = 0; j < n_radius+1; j++){
                nu_r.at(j*n_theta + i) = nu_column_int.at(j);
            }
        }
        vector<double> nu_t;
        for(int i = 0; i < n_radius; i++){
            vector<double> nu_row;
            for(int j = 0; j < n_theta; j++){
                nu_row.push_back(nu.at(i*n_theta + j));
            }
            vector<double> nu_row_int = interp1(nu_row, true);
            nu_t.insert(nu_t.end(), nu_row_int.begin(), nu_row_int.end());
        }
    
    //------------geometry coefficients
    
    vector<double> rig;            // radial inwards coefficients
    vector<double> rog;            // radial outwards coefficients
    vector<double> tdg;            // polar downwards coefficients
    vector<double> tug;            // polar upwards coefficients
    vector<double> radial_grid_int = interp1(radial_grid, true);
    vector<double> polar_grid_int = interp1(polar_grid, true);
    
    for(int i = 0; i< n_radius; i++){
        for(int j = 0; j < n_theta; j++){
            rig.push_back(pow(radial_grid_int.at(i),2)/((1./3.)*(pow(radial_grid_int.at(i+1),3) - pow(radial_grid_int.at(i),3))*dr));
            rog.push_back(pow(radial_grid_int.at(i+1),2)/((1./3.)*(pow(radial_grid_int.at(i+1),3) - pow(radial_grid_int.at(i),3))*dr));
            tdg.push_back(1./pow(radial_grid.at(i),2) * (abs(sin(polar_grid_int.at(j)))/(abs(cos(polar_grid_int.at(j))-cos(polar_grid_int.at(j+1)))*dtheta)));
            tug.push_back(1./pow(radial_grid.at(i),2) * (abs(sin(polar_grid_int.at(j+1)))/(abs(cos(polar_grid_int.at(j))-cos(polar_grid_int.at(j+1)))*dtheta)));
            
        }
    }
    
    //--------------build matrix
    
    // matrix vector Ax2
    Ax2.at(0) =  1+ dustDiffTime*nu_r.at(0+n_theta) *rog.at(0) +dustDiffTime*nu_r.at(0)*rig.at(0) + dustDiffTime*nu_t.at(0+1)*tug.at(0) +dustDiffTime*nu_t.at(0)*tdg.at(0);                                                                                                         // diffusion equation rho(i,j)
    Ax2.at(1) = -dustDiffTime*nu_t.at(1)*tdg.at(1);                                                                                                                                                                                                                                 // diffusion equation rho(i,j-1)
    Ax2.at(2) = -dustDiffTime*nu_r.at(n_theta)*rig.at(n_theta);                                                                                                                                                                                                                     // diffusion equation rho(i-1,j)
    int j = 3;
    for(int i = 1; i < n_theta; i++){
        Ax2.at(j) = -dustDiffTime*nu_t.at(i)*tug.at(i-1);                                                                                                                                                                                                                           // diffusion equation rho(i,j+1)
        j++;
        Ax2.at(j) = 1+ dustDiffTime*nu_r.at(i+n_theta) *rog.at(i) +dustDiffTime*nu_r.at(i)*rig.at(i) + dustDiffTime*nu_t.at(i+1)*tug.at(i) +dustDiffTime*nu_t.at(i)*tdg.at(i);                                                                                                      // diffusion equation rho(i,j)
        j++;
        Ax2.at(j) = -dustDiffTime*nu_t.at(i+1)*tdg.at(i+1);                                                                                                                                                                                                                         // diffusion equation rho(i,j-1)
        j++;
        Ax2.at(j) = -dustDiffTime*nu_r.at(i+n_theta)*rig.at(i+n_theta);                                                                                                                                                                                                             // diffusion equation rho(i-1,j)
        j++;
    }
    for(int i = 1; i < n_radius-1; i++){
        for(int k = 0; k < n_theta; k++){
            Ax2.at(j) = -dustDiffTime*nu_r.at(i*n_theta +k)*rog.at(i*n_theta +k - n_theta);                                                                                                                                                                                         // diffusion equation rho(i+1,j)
            j++;
            Ax2.at(j) = -dustDiffTime*nu_t.at(i*(n_theta+1) +k)*tug.at(i*n_theta +k -1);                                                                                                                                                                                            // diffusion equation rho(i,j+1)
            j++;
            Ax2.at(j) = 1+ dustDiffTime*nu_r.at(i*n_theta +k +n_theta) *rog.at(i*n_theta +k) +dustDiffTime*nu_r.at(i*n_theta +k)*rig.at(i*n_theta +k) + dustDiffTime*nu_t.at(i*(n_theta+1) +k+1)*tug.at(i*n_theta +k) +dustDiffTime*nu_t.at(i*(n_theta+1) +k)*tdg.at(i*n_theta +k); // diffusion equation rho(i,j)
            j++;
            Ax2.at(j) = -dustDiffTime*nu_t.at(i*(n_theta+1) +k +1)*tdg.at(i*n_theta +k+1);                                                                                                                                                                                          // diffusion equation rho(i,j-1);
            j++;
            Ax2.at(j) = -dustDiffTime*nu_r.at(i*n_theta +k +n_theta)*rig.at(i*n_theta +k +n_theta);                                                                                                                                                                                 // diffusion equation rho(i-1,j)
            j++;
        }
    }
    for(int k = 0; k < n_theta-1; k++){
        int i = n_radius-1;
        Ax2.at(j) = -dustDiffTime*nu_r.at(i*n_theta +k)*rog.at(i*n_theta +k - n_theta);                                                                                                                                                                                             // diffusion equation rho(i+1,j)
        j++;
        Ax2.at(j) = -dustDiffTime*nu_t.at(i*(n_theta+1) +k)*tug.at(i*n_theta +k -1);                                                                                                                                                                                                // diffusion equation rho(i,j+1)
        j++;
        Ax2.at(j) = 1+ dustDiffTime*nu_r.at(i*n_theta +k +n_theta)*rog.at(i*n_theta +k) + dustDiffTime*nu_r.at(i*n_theta +k)*rig.at(i*n_theta +k) + dustDiffTime*nu_t.at(i*(n_theta+1) +k+1)*tug.at(i*n_theta +k) +dustDiffTime*nu_t.at(i*(n_theta+1) +k)*tdg.at(i*n_theta +k);     // diffusion equation rho(i,j)
        j++;
        Ax2.at(j) = -dustDiffTime*nu_t.at(i*(n_theta+1) +k +1)*tdg.at(i*n_theta +k+1);                                                                                                                                                                                              // diffusion equation rho(i,j-1);
        j++;
    }
    int i = n_radius-1;
    int k = n_theta-1;
    Ax2.at(j) = -dustDiffTime*nu_r.at(i*n_theta +k)*rog.at(i*n_theta +k - n_theta);                                                                                                                                                                                                 // diffusion equation rho(i+1,j)
    j++;
    Ax2.at(j) = -dustDiffTime*nu_t.at(i*(n_theta+1) +k)*tug.at(i*n_theta +k -1);                                                                                                                                                                                                    // diffusion equation rho(i,j+1)
    j++;
    Ax2.at(j) = 1+ dustDiffTime*nu_r.at(i*n_theta +k +n_theta) *rog.at(i*n_theta +k) +dustDiffTime*nu_r.at(i*n_theta +k)*rig.at(i*n_theta +k) + dustDiffTime*nu_t.at(i*(n_theta+1) +k+1)*tug.at(i*n_theta +k) +dustDiffTime*nu_t.at(i*(n_theta+1) +k)*tdg.at(i*n_theta +k);         // diffusion equation rho(i,j)
    
   
    //----------------boundary conditions
    
    // remove periodic BC
    int p = 3+ 4*(n_theta-1);                                         // number of matrix entries before upper diagonal sets in
    Ax2.at(p-2) = 0;
    for(int i = 0; i < n_radius-2; i++){
        Ax2.at(p+1+i*(5*n_theta)) = 0;
        Ax2.at(p-2+(i+1)*(5*n_theta)) = 0;
    }
    int q = 3+ 4*(n_theta-1)+ 5*(n_radius-2)*n_theta;                // number of matrix entries before lower diagonal stops
    Ax2.at(q+1) = 0;
    
    // zero gradient for f_d2g
        // inner radius
    for(int i=0; i<n_theta; i++){
        Ax2.at(i*4) += -dustDiffTime*nu_r.at(i)*rig.at(i);
    }
        // outer radius
    i = n_radius-1;
    for(int k=0; k<n_theta; k++){
        Ax2.at(q+2 +k*4) += -dustDiffTime*nu_r.at(i*n_theta +k +n_theta) *rog.at(i*n_theta +k);
    }
        // lower theta
    Ax2.at(0) += -dustDiffTime*nu_t.at(0)*tdg.at(0); 
    k = 0;
    for(int i=1; i<n_radius; i++){
        Ax2.at(p+2 + (i-1)*n_theta*5) += -dustDiffTime*nu_t.at(i*(n_theta+1) +k)*tdg.at(i*n_theta +k);
    }
        // upper theta
    Ax2.at(p- 3) += -dustDiffTime*nu_t.at(n_theta)*tug.at(n_theta-1);
    k= n_theta-1;
    for(int i=1; i<n_radius -1; i++){
        Ax2.at(p-3 + i*n_theta*5) += -dustDiffTime*nu_t.at(i*(n_theta+1) +k+1)*tug.at(i*n_theta +k);
    }
    i= n_radius-1;
    Ax2.at(q+2 +k*4) += -dustDiffTime*nu_t.at(i*(n_theta+1) +k+1)*tug.at(i*n_theta +k);
    
    /*
    vector<double> AxDouble(Ax2.begin(), Ax2.end());
    
    stringstream Axfilename;
    Axfilename << "AxDust.dat";
    int n_entries2 = 2*3 + 2*(n_theta - 1)*4 + (n_theta*n_radius - 2*n_theta)*5;
    write2d(Axfilename.str(), AxDouble, n_entries2, 1);
    */
    
    //---------------invert the gas and dust diffusion matrix
    
    //status = umfpack_di_report_matrix ( matrix_size, matrix_size, &Ap[0], &Ai[0], &Ax[0], 1, Control);
    //cout << "status matrix: " << status << endl;
    //umfpack_di_report_status (Control, status) ;

    //
    //  Carry out the symbolic factorization.
    //
    status = umfpack_di_symbolic ( matrix_size2, matrix_size2, &Ap2[0], &Ai2[0], &Ax2[0], &Symbolic, Control, Info );
    //cout << "status 1: " << status << endl;
    umfpack_di_report_status (Control, status) ;
    //
    //  Use the symbolic factorization to carry out the numeric factorization.
    //
    status = umfpack_di_numeric (&Ap2[0], &Ai2[0], &Ax2[0], Symbolic, &Numeric, Control, Info );
    //cout << "status 2: " << status << endl;
    umfpack_di_report_status (Control, status) ;
    //
    //  Free the memory associated with the symbolic factorization.
    //
    umfpack_di_free_symbolic ( &Symbolic );
    //
    //  Solve the linear system.
    //
    status = umfpack_di_solve ( UMFPACK_A, &Ap2[0], &Ai2[0], &Ax2[0], &x2[0], &b[0], Numeric, Control, Info );
    //cout << "status 3: " << status << endl;
    umfpack_di_report_status (Control, status) ;
    //
    //  Rearrange x into dens
    //
    for(int i = 0; i < size; i++){
        dens.at(i) = x2.at(i);
    }
    //
    //  Solve the other linear system
    //
    status = umfpack_di_solve ( UMFPACK_A, &Ap2[0], &Ai2[0], &Ax2[0], &x2[0], &c[0], Numeric, Control, Info );
    //cout << "status 3: " << status << endl;
    umfpack_di_report_status (Control, status) ;
    //
    //  Free the memory associated with the numeric factorization.
    //
    umfpack_di_free_numeric ( &Numeric );
    //
    //  Rearrange x into f_d2g and ensure boundaries
    //
    for(int i = 0; i < size; i++){
        f_d2g.at(i)= x2.at(i)/dens.at(i);
        if(f_d2g.at(i)< f_d2g_min){
            f_d2g.at(i) = f_d2g_min;
        }
        if(f_d2g.at(i)> f_d2g_0){
            f_d2g.at(i) = f_d2g_0;
        }
    }
    
    return;
}
