INCLUDES = -I /home/btpp/btp00000/shared/cJSON -I /home/btpp/btp00000/shared/SuiteSparse2-current/include
CXXFLAGS = -g -Wall -std=c++14 -O3 -fomit-frame-pointer -floop-parallelize-all -fforce-addr -freg-struct-return -fprefetch-loop-arrays -march=native ${INCLUDES}
OBJS     = rmhd_code.o
PROG     = run
LIBS     = -L /home/btpp/btp00000/shared/cJSON/lib -lcjson -L /home/btpp/btp00000/shared/SuiteSparse2-current/lib -lumfpack -lamd -lsuitesparseconfig



all:            $(PROG)

${PROG}:        $(OBJS)
	$(CXX) $(INCLUDES) -o $(PROG) $(OBJS) $(LIBS)
clean:
	$(RM) -f $(PROG) core *.o
