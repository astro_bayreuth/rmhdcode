
densTest = load('densTest.dat');
f_d2gTest = load('f_d2gTest.dat');
ErTest = load('ErTest.dat');
tempTest = load('tempTest.dat');

dens = load('3dens.dat');
f_d2g = load('3f_d2g.dat');
Er = load('3Er.dat');
temp = load('3temp.dat');

densComp = densTest - dens;
f_d2gComp = f_d2gTest - f_d2g;
ErComp = ErTest - Er;
tempComp = tempTest - temp;

densDiff = sum(sum(densComp))
f_d2gDiff = sum(sum(f_d2gComp))
ErDiff = sum(sum(ErComp))
tempDiff = sum(sum(tempComp))

if((densDiff + f_d2gDiff + ErDiff + tempDiff) == 0)
    disp("Test was successful, no alteration found.");
else
    disp("Test was unsuccessful, the code is compromised.");
endif
